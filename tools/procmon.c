#include <libusage.h>

static void
items_changed_cb (GListModel *model,
                  guint       position,
                  guint       removed,
                  guint       added,
                  gpointer    user_data)
{
  if (removed)
    g_print ("%d removed\n", removed);

  for (guint i = 0; i < added; i++)
    {
      g_autoptr(UsageProcess) p = g_list_model_get_item (model, position + i);

      g_print ("+ %d\n", usage_process_get_pid (p));
    }
}

gint
main (gint argc,
      gchar *argv[])
{
  usage_init ();
  {
    GMainLoop *main_loop = g_main_loop_new (NULL, FALSE);
    g_autoptr(UsageProcessMonitor) monitor = usage_process_monitor_new ();
    g_signal_connect (monitor,
                      "items-changed",
                      G_CALLBACK (items_changed_cb),
                      NULL);
    usage_process_monitor_hold (monitor);
    g_main_loop_run (main_loop);
  }
  usage_shutdown ();

  return 0;
}
