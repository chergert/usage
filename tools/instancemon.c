#include <libusage.h>

static gboolean
poll_instances (gpointer data)
{
  GListModel *instances = data;
  guint n_items = g_list_model_get_n_items (instances);

  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(UsageAppInstance) instance = g_list_model_get_item (instances, i);

      g_print ("%d: %32s: %s\n",
               usage_app_instance_get_pid (instance),
               usage_app_instance_get_id (instance),
               usage_app_instance_get_app_id (instance));
    }

  g_print ("==========\n");

  return G_SOURCE_CONTINUE;
}

gint
main (gint argc,
      gchar *argv[])
{
  usage_init ();
  {
    GMainLoop *main_loop = g_main_loop_new (NULL, FALSE);
    UsageSystem *sys = usage_system_get_default ();
    g_autoptr(GListModel) instances = usage_system_get_app_instances (sys);

    g_timeout_add_seconds (1, poll_instances, instances);
    g_main_loop_run (main_loop);
  }
  usage_shutdown ();

  return 0;
}
