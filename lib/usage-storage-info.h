/* usage-storage-info.h
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define USAGE_TYPE_STORAGE_INFO (usage_storage_info_get_type())

typedef enum _UsageStorageKind
{
  USAGE_STORAGE_KIND_UNKNOWN = 0,
  USAGE_STORAGE_KIND_INTERNAL,
  USAGE_STORAGE_KIND_REMOVABLE,
} UsageStorageKind;

G_DECLARE_DERIVABLE_TYPE (UsageStorageInfo, usage_storage_info, USAGE, STORAGE_INFO, GObject)

struct _UsageStorageInfoClass
{
  GObjectClass parent_class;

  /*< private >*/
  gpointer _reserved[8];
};

const char *usage_storage_info_get_name      (UsageStorageInfo *self);
void        usage_storage_info_set_name      (UsageStorageInfo *self,
                                              const char       *name);
const char *usage_storage_info_get_title     (UsageStorageInfo *self);
void        usage_storage_info_set_title     (UsageStorageInfo *self,
                                              const char       *title);
const char *usage_storage_info_get_icon_name (UsageStorageInfo *self);
void        usage_storage_info_set_icon_name (UsageStorageInfo *self,
                                              const char       *icon_name);
gboolean    usage_storage_info_get_encrypted (UsageStorageInfo *self);
void        usage_storage_info_set_encrypted (UsageStorageInfo *self,
                                              gboolean          encrypted);

G_END_DECLS
