/* usage-process-monitor.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "usage-process.h"
#include "usage-process-monitor.h"
#include "usage-system.h"

#define PROCESS(pid) ((Process){.pid=pid})

typedef struct
{
  UsageProcess *process;     /* Lazily created */
  GPid          pid;         /* Used for set compare */
} Process;

struct _UsageProcessMonitor
{
  GObject  parent_instance;

  /* An array of Process structures. Only the @pid field is guarnateed to be
   * filled in. This array is updated after each call to
   * usage_system_list_pids() while we do a sort/merge into a new array (but
   * preserving the UsageProcess as it is stateful).
   */
  GArray  *items;
  GSequence *procs;

  /* The interval (in milliseconds) to poll for new processes */
  guint    poll_interval;

  /* A GSource identifier to use with g_source_remove() */
  guint    poll_handler;

  /* We keep track of how many components are using the process monitor so that
   * we can dynamically stop polling for process state if nothing is requesting
   * it for display.
   */
  gint     hold_count;
};

static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (UsageProcessMonitor, usage_process_monitor, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

enum {
  PROP_0,
  PROP_POLL_INTERVAL,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
process_free (Process *process)
{
  g_clear_object (&process->process);
  g_slice_free (Process, process);
}

static void
usage_process_monitor_finalize (GObject *object)
{
  UsageProcessMonitor *self = (UsageProcessMonitor *)object;

  g_clear_pointer (&self->procs, g_sequence_free);
  g_clear_handle_id (&self->poll_handler, g_source_remove);

  G_OBJECT_CLASS (usage_process_monitor_parent_class)->finalize (object);
}

static void
usage_process_monitor_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  UsageProcessMonitor *self = USAGE_PROCESS_MONITOR (object);

  switch (prop_id)
    {
    case PROP_POLL_INTERVAL:
      g_value_set_uint (value, usage_process_monitor_get_poll_interval (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
usage_process_monitor_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  UsageProcessMonitor *self = USAGE_PROCESS_MONITOR (object);

  switch (prop_id)
    {
    case PROP_POLL_INTERVAL:
      usage_process_monitor_set_poll_interval (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
usage_process_monitor_class_init (UsageProcessMonitorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = usage_process_monitor_finalize;
  object_class->get_property = usage_process_monitor_get_property;
  object_class->set_property = usage_process_monitor_set_property;

  properties [PROP_POLL_INTERVAL] =
    g_param_spec_uint ("poll-interval",
                       "Poll Interval",
                       "The interval to polling for new processes",
                       1, G_MAXUINT, 1000,
                       (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  
  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
usage_process_monitor_init (UsageProcessMonitor *self)
{
  self->poll_interval = 1000;
  self->procs = g_sequence_new ((GDestroyNotify)process_free);
}

static gint
sort_by_pid (const GPid *pid_a,
             const GPid *pid_b)
{
  if (*pid_a < *pid_b)
    return -1;
  else if (*pid_a > *pid_b)
    return 1;
  else
    return 0;
}

static gboolean
usage_process_monitor_poll_cb (gpointer user_data)
{
  UsageProcessMonitor *self = user_data;
  UsageSystem *sys = usage_system_get_default ();
  g_autoptr(GArray) pids = NULL;
  GSequenceIter *end;
  GSequenceIter *iter;
  guint j = 0;

  g_assert (USAGE_IS_PROCESS_MONITOR (self));

  if (!(pids = usage_system_list_pids (sys, NULL)))
    goto skip;

  end = g_sequence_get_end_iter (self->procs);
  iter = g_sequence_get_begin_iter (self->procs);

  g_array_sort (pids, (GCompareFunc)sort_by_pid);

  for (guint i = 0; i < pids->len; i++)
    {
      GPid pid = g_array_index (pids, GPid, i);

      while (iter != end)
        {
          Process *p = g_sequence_get (iter);
          GSequenceIter *next;

          if (p->pid > pid)
            break;

          next = g_sequence_iter_next (iter);

          if (p->pid == pid)
            {
              j++;
              iter = next;
              goto skip_insert;
            }

          g_assert (p->pid < pid);

          g_sequence_remove (iter);
          iter = next;
          g_list_model_items_changed (G_LIST_MODEL (self), j, 1, 0);
        }

      g_sequence_insert_before (iter, g_slice_dup (Process, &PROCESS (pid)));
      g_list_model_items_changed (G_LIST_MODEL (self), j, 0, 1);
      j++;

    skip_insert:
      continue;
    }

skip:
  return G_SOURCE_CONTINUE;
}

static void
usage_process_monitor_start (UsageProcessMonitor *self)
{
  g_assert (USAGE_IS_PROCESS_MONITOR (self));
  g_assert (self->poll_handler == 0);

  self->poll_handler = g_timeout_add_full (G_PRIORITY_DEFAULT,
                                           self->poll_interval,
                                           usage_process_monitor_poll_cb,
                                           self, NULL);

  if (!usage_process_monitor_poll_cb (self))
    g_clear_handle_id (&self->poll_handler, g_source_remove);
}

static void
usage_process_monitor_stop (UsageProcessMonitor *self)
{
  guint len;

  g_assert (USAGE_IS_PROCESS_MONITOR (self));
  g_assert (self->poll_handler != 0);

  g_clear_handle_id (&self->poll_handler, g_source_remove);

  len = g_sequence_get_length (self->procs);

  if (len > 0)
    {
      g_sequence_remove_range (g_sequence_get_begin_iter (self->procs),
                               g_sequence_get_end_iter (self->procs));
      g_list_model_items_changed (G_LIST_MODEL (self), 0, len, 0);
    }
}

UsageProcessMonitor *
usage_process_monitor_new (void)
{
  return g_object_new (USAGE_TYPE_PROCESS_MONITOR, NULL);
}

void
usage_process_monitor_hold (UsageProcessMonitor *self)
{
  g_return_if_fail (USAGE_IS_PROCESS_MONITOR (self));

  self->hold_count++;

  if (self->hold_count == 1)
    usage_process_monitor_start (self);
}

void
usage_process_monitor_release (UsageProcessMonitor *self)
{
  g_return_if_fail (USAGE_IS_PROCESS_MONITOR (self));
  g_return_if_fail (self->hold_count > 0);

  self->hold_count--;

  if (self->hold_count == 0)
    usage_process_monitor_stop (self);
}

static guint
usage_process_monitor_get_n_items (GListModel *model)
{
  return USAGE_PROCESS_MONITOR (model)->items->len;
}

static GType
usage_process_monitor_get_item_type (GListModel *model)
{
  return USAGE_TYPE_PROCESS;
}

static gpointer
usage_process_monitor_get_item (GListModel *model,
                                guint       position)
{
  UsageProcessMonitor *self = USAGE_PROCESS_MONITOR (model);
  GSequenceIter *iter = g_sequence_get_iter_at_pos (self->procs, position);
  Process *p = g_sequence_get (iter);

  if (p->process == NULL)
    p->process = usage_process_new_for_pid (p->pid);

  return g_object_ref (p->process);
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_n_items = usage_process_monitor_get_n_items;
  iface->get_item_type = usage_process_monitor_get_item_type;
  iface->get_item = usage_process_monitor_get_item;
}

guint
usage_process_monitor_get_poll_interval (UsageProcessMonitor *self)
{
  g_return_val_if_fail (USAGE_IS_PROCESS_MONITOR (self), 0);

  return self->poll_interval;
}

void
usage_process_monitor_set_poll_interval (UsageProcessMonitor *self,
                                         guint                poll_interval)
{
  g_return_if_fail (USAGE_IS_PROCESS_MONITOR (self));
  g_return_if_fail (poll_interval > 0);

  if (poll_interval == self->poll_interval)
    return;

  self->poll_interval = poll_interval;

  if (self->hold_count > 0)
    {
      usage_process_monitor_stop (self);
      usage_process_monitor_start (self);
    }

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_POLL_INTERVAL]);
}
