/* usage-process.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <glib/gi18n.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include "usage-process.h"

#ifdef __linux__
# include "usage-linux-process.h"
#endif

G_DEFINE_INTERFACE (UsageProcess, usage_process, G_TYPE_OBJECT)

static void
usage_process_real_force_quit_async (UsageProcess        *self,
                                     GCancellable        *cancellable,
                                     GAsyncReadyCallback  callback,
                                     gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  GPid pid;

  g_assert (USAGE_IS_PROCESS (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, usage_process_real_force_quit_async);

  pid = usage_process_get_pid (self);

  if (pid > 0 && pid != getpid ())
    {
      kill (pid, SIGTERM);
      g_task_return_boolean (task, TRUE);
    }
  else
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_FAILED,
                               "%s",
                               _("Process cannot be forced to quit"));
    }
}

static gboolean
usage_process_real_force_quit_finish (UsageProcess  *self,
                                      GAsyncResult  *result,
                                      GError       **error)
{
  g_assert (USAGE_IS_PROCESS (self));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
usage_process_default_init (UsageProcessInterface *iface)
{
  iface->force_quit_async = usage_process_real_force_quit_async;
  iface->force_quit_finish = usage_process_real_force_quit_finish;
}

UsageProcess *
usage_process_new_for_pid (GPid pid)
{
#ifdef __linux__
  return usage_linux_process_new_for_pid (pid);
#else
# error "Your system is not supported. Please implement usage_process_new_for_pid()"
#endif
}

gboolean
usage_process_poll_info (UsageProcess      *self,
                         UsageProcessInfo  *info,
                         GError           **error)
{
  g_return_val_if_fail (USAGE_IS_PROCESS (self), FALSE);
  g_return_val_if_fail (info != NULL, FALSE);

  return USAGE_PROCESS_GET_IFACE (self)->poll_info (self, info, error);
}

void
usage_process_force_quit_async (UsageProcess        *self,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  g_return_if_fail (USAGE_IS_PROCESS (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  USAGE_PROCESS_GET_IFACE (self)->force_quit_async (self, cancellable, callback, user_data);
}

gboolean
usage_process_force_quit_finish (UsageProcess  *self,
                                 GAsyncResult  *result,
                                 GError       **error)
{
  g_return_val_if_fail (USAGE_IS_PROCESS (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return USAGE_PROCESS_GET_IFACE (self)->force_quit_finish (self, result, error);
}

GPid
usage_process_get_pid (UsageProcess *self)
{
  g_return_val_if_fail (USAGE_IS_PROCESS (self), 0);

  return USAGE_PROCESS_GET_IFACE (self)->get_pid (self);
}
