/* usage-app-instance-provider.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "usage-app-instance-provider.h"

G_DEFINE_INTERFACE (UsageAppInstanceProvider, usage_app_instance_provider, G_TYPE_OBJECT)

enum {
  INSTANCE_ADDED,
  INSTANCE_REMOVED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

static void
usage_app_instance_provider_default_init (UsageAppInstanceProviderInterface *iface)
{
  signals [INSTANCE_ADDED] =
    g_signal_new ("instance-added",
                  G_TYPE_FROM_INTERFACE (iface),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (UsageAppInstanceProviderInterface, instance_added),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, USAGE_TYPE_APP_INSTANCE);

  signals [INSTANCE_REMOVED] =
    g_signal_new ("instance-removed",
                  G_TYPE_FROM_INTERFACE (iface),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (UsageAppInstanceProviderInterface, instance_removed),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, USAGE_TYPE_APP_INSTANCE);
}

void
usage_app_instance_provider_instance_added (UsageAppInstanceProvider *self,
                                            UsageAppInstance         *instance)
{
  g_return_if_fail (USAGE_IS_APP_INSTANCE_PROVIDER (self));
  g_return_if_fail (USAGE_IS_APP_INSTANCE (instance));

  g_signal_emit (self, signals [INSTANCE_ADDED], 0, instance);
}

void
usage_app_instance_provider_instance_removed (UsageAppInstanceProvider *self,
                                              UsageAppInstance         *instance)
{
  g_return_if_fail (USAGE_IS_APP_INSTANCE_PROVIDER (self));
  g_return_if_fail (USAGE_IS_APP_INSTANCE (instance));

  g_signal_emit (self, signals [INSTANCE_REMOVED], 0, instance);
}
