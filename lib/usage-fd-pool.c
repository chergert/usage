/* usage-fd-pool.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include "usage-fd-pool-private.h"

#define DEFAULT_MAX_FD 512

static GHashTable *fd_pool_hash;
static GQueue      fd_pool_queue;
static guint       fd_pool_max;

typedef struct
{
  GList  link;
  gint   fd;
  guint8 active : 1;
  gchar  path[0];
} PoolEntry;

void
usage_fd_pool_init (guint max_size)
{
  if (g_once_init_enter (&fd_pool_hash))
    {
      GHashTable *ht;

      fd_pool_max = MAX (max_size, DEFAULT_MAX_FD);
      ht = g_hash_table_new (g_str_hash, g_str_equal);
      g_once_init_leave (&fd_pool_hash, ht);
    }
}

static gboolean
usage_fd_pool_pop (void)
{
  PoolEntry *tail;

  g_assert (fd_pool_queue.length == 0 || fd_pool_queue.head != NULL);
  g_assert (fd_pool_queue.length > 0 || fd_pool_queue.head == NULL);

  if (g_queue_is_empty (&fd_pool_queue))
    return FALSE;

  tail = g_queue_peek_tail (&fd_pool_queue);

  g_assert (tail != NULL);

  if (tail->active)
    return FALSE;

  g_hash_table_remove (fd_pool_hash, tail->path);
  g_queue_unlink (&fd_pool_queue, &tail->link);
  close (tail->fd);
  g_slice_free1 (sizeof *tail + strlen (tail->path) + 1, tail);

  return TRUE;
}

gint
usage_fd_pool_get (const gchar *path,
                   guint        oflags,
                   gboolean     reset_position)
{
  PoolEntry *entry;
  gsize len = strlen (path);

  g_assert (len < 1024);

  if ((entry = g_hash_table_lookup (fd_pool_hash, path)))
    {
      /* We only store one in the pool */
      if (entry->active)
        goto open_no_pooling;

      entry->active = TRUE;

      g_queue_unlink (&fd_pool_queue, &entry->link);
      g_queue_push_head_link (&fd_pool_queue, &entry->link);

      if (reset_position)
        lseek (entry->fd, 0, SEEK_SET);

      return entry->fd;
    }

  if (fd_pool_queue.length >= fd_pool_max)
    {
      if (!usage_fd_pool_pop ())
        goto open_no_pooling;
    }

  entry = g_slice_alloc0 (sizeof *entry + len + 1);
  entry->link.data = entry;
  entry->active = TRUE;
  entry->fd = open (path, oflags);
  memcpy (entry->path, path, len);
  entry->path[len] = 0;

  g_queue_push_head_link (&fd_pool_queue, &entry->link);
  g_hash_table_insert (fd_pool_hash, entry->path, entry);

  return entry->fd;

open_no_pooling:
  return open (path, oflags);
}

void
usage_fd_pool_put (const gchar *path,
                   gint         fd)
{
  PoolEntry *entry;

  if ((entry = g_hash_table_lookup (fd_pool_hash, path)))
    {
      if (entry->fd == fd)
        {
          entry->active = FALSE;
          g_queue_unlink (&fd_pool_queue, &entry->link);
          g_queue_push_tail_link (&fd_pool_queue, &entry->link);
        }
    }
  else
    {
      close (fd);
    }
}

void
usage_fd_pool_shutdown (void)
{
  while (fd_pool_queue.length > 0)
    {
      if (!usage_fd_pool_pop ())
        break;
    }
}

void
usage_fd_pool_release (const gchar *path)
{
  PoolEntry *entry;

  if ((entry = g_hash_table_lookup (fd_pool_hash, path)))
    {
      if (!entry->active)
        {
          g_hash_table_remove (fd_pool_hash, entry->path);
          g_queue_unlink (&fd_pool_queue, &entry->link);
          close (entry->fd);
          g_slice_free1 (sizeof *entry + strlen (entry->path) + 1, entry);
        }
    }
}
