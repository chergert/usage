/* usage-storage-device.c
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "usage-storage-device"

#include "config.h"

#include "usage-storage-device.h"

G_DEFINE_INTERFACE (UsageStorageDevice, usage_storage_device, G_TYPE_OBJECT)

static void
usage_storage_device_real_load_info_async (UsageStorageDevice  *self,
                                           GCancellable        *cancellable,
                                           GAsyncReadyCallback  callback,
                                           gpointer             user_data)
{
  g_task_report_new_error (self, callback, user_data,
                           usage_storage_device_real_load_info_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Not supported");
}

static UsageStorageInfo *
usage_storage_device_real_load_info_finish (UsageStorageDevice  *self,
                                            GAsyncResult        *result,
                                            GError             **error)
{
  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
usage_storage_device_default_init (UsageStorageDeviceInterface *iface)
{
  iface->load_info_async = usage_storage_device_real_load_info_async;
  iface->load_info_finish = usage_storage_device_real_load_info_finish;
}

void
usage_storage_device_load_info_async (UsageStorageDevice  *self,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  g_return_if_fail (USAGE_IS_STORAGE_DEVICE (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  USAGE_STORAGE_DEVICE_GET_IFACE (self)->load_info_async (self, cancellable, callback, user_data);
}

UsageStorageInfo *
usage_storage_device_load_info_finish (UsageStorageDevice  *self,
                                       GAsyncResult        *result,
                                       GError             **error)
{
  g_return_val_if_fail (USAGE_IS_STORAGE_DEVICE (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  return USAGE_STORAGE_DEVICE_GET_IFACE (self)->load_info_finish (self, result, error);
}
