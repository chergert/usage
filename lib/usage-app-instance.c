/* usage-app-instance.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <glib/gi18n.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>

#include "usage-app-instance.h"

typedef struct
{
  gchar *id;
  gchar *app_id;
  GPid   pid;
} UsageAppInstancePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (UsageAppInstance, usage_app_instance, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_APP_ID,
  PROP_ID,
  PROP_PID,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

UsageAppInstance *
usage_app_instance_new (const gchar *id)
{
  return g_object_new (USAGE_TYPE_APP_INSTANCE,
                       "id", id,
                       NULL);
}

static void
usage_app_instance_real_force_quit_async (UsageAppInstance    *self,
                                          GCancellable        *cancellable,
                                          GAsyncReadyCallback  callback,
                                          gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  GPid pid;

  g_assert (USAGE_IS_APP_INSTANCE (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, usage_app_instance_real_force_quit_async);

  pid = usage_app_instance_get_pid (self);

  if (pid > 0 && pid != getpid ())
    {
      kill (pid, SIGTERM);
      g_task_return_boolean (task, TRUE);
    }
  else
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_FAILED,
                               "%s",
                               _("Process cannot be forced to quit"));
    }
}

static gboolean
usage_app_instance_real_force_quit_finish (UsageAppInstance  *self,
                                           GAsyncResult      *result,
                                           GError           **error)
{
  g_assert (USAGE_IS_APP_INSTANCE (self));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
usage_app_instance_finalize (GObject *object)
{
  UsageAppInstance *self = (UsageAppInstance *)object;
  UsageAppInstancePrivate *priv = usage_app_instance_get_instance_private (self);

  g_clear_pointer (&priv->app_id, g_free);
  g_clear_pointer (&priv->id, g_free);

  G_OBJECT_CLASS (usage_app_instance_parent_class)->finalize (object);
}

static void
usage_app_instance_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  UsageAppInstance *self = USAGE_APP_INSTANCE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, usage_app_instance_get_id (self));
      break;

    case PROP_APP_ID:
      g_value_set_string (value, usage_app_instance_get_app_id (self));
      break;

    case PROP_PID:
      g_value_set_int (value, usage_app_instance_get_pid (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
usage_app_instance_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  UsageAppInstance *self = USAGE_APP_INSTANCE (object);
  UsageAppInstancePrivate *priv = usage_app_instance_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ID:
      priv->id = g_value_dup_string (value);
      break;

    case PROP_APP_ID:
      usage_app_instance_set_app_id (self, g_value_get_string (value));
      break;

    case PROP_PID:
      usage_app_instance_set_pid (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
usage_app_instance_class_init (UsageAppInstanceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = usage_app_instance_finalize;
  object_class->get_property = usage_app_instance_get_property;
  object_class->set_property = usage_app_instance_set_property;

  klass->force_quit_async = usage_app_instance_real_force_quit_async;
  klass->force_quit_finish = usage_app_instance_real_force_quit_finish;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "The identifier for the instance",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  properties [PROP_APP_ID] =
    g_param_spec_string ("app-id",
                         "App Id",
                         "App Identifier",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_PID] =
    g_param_spec_int ("pid",
                      "Pid",
                      "The PID of the parent process for the instance",
                      -1, G_MAXINT, -1,
                      (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
usage_app_instance_init (UsageAppInstance *self)
{
}

const gchar *
usage_app_instance_get_id (UsageAppInstance *self)
{
  UsageAppInstancePrivate *priv = usage_app_instance_get_instance_private (self);

  g_return_val_if_fail (USAGE_IS_APP_INSTANCE (self), NULL);

  return priv->id;
}

const gchar *
usage_app_instance_get_app_id (UsageAppInstance *self)
{
  UsageAppInstancePrivate *priv = usage_app_instance_get_instance_private (self);

  g_return_val_if_fail (USAGE_IS_APP_INSTANCE (self), NULL);

  return priv->app_id;
}

void
usage_app_instance_set_app_id (UsageAppInstance *self,
                               const gchar      *app_id)
{
  UsageAppInstancePrivate *priv = usage_app_instance_get_instance_private (self);

  g_return_if_fail (USAGE_IS_APP_INSTANCE (self));

  if (g_strcmp0 (app_id, priv->app_id) != 0)
    {
      g_free (priv->app_id);
      priv->app_id = g_strdup (app_id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_APP_ID]);
    }
}

GPid
usage_app_instance_get_pid (UsageAppInstance *self)
{
  UsageAppInstancePrivate *priv = usage_app_instance_get_instance_private (self);

  g_return_val_if_fail (USAGE_IS_APP_INSTANCE (self), 0);

  return priv->pid;
}

void
usage_app_instance_set_pid (UsageAppInstance *self,
                            GPid              pid)
{
  UsageAppInstancePrivate *priv = usage_app_instance_get_instance_private (self);

  g_return_if_fail (USAGE_IS_APP_INSTANCE (self));

  if (pid != priv->pid)
    {
      priv->pid = pid;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PID]);
    }
}

void
usage_app_instance_force_quit_async (UsageAppInstance    *self,
                                     GCancellable        *cancellable,
                                     GAsyncReadyCallback  callback,
                                     gpointer             user_data)
{
  g_return_if_fail (USAGE_IS_APP_INSTANCE (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  USAGE_APP_INSTANCE_GET_CLASS (self)->force_quit_async (self, cancellable, callback, user_data);
}

gboolean
usage_app_instance_force_quit_finish (UsageAppInstance  *self,
                                      GAsyncResult      *result,
                                      GError           **error)
{
  g_return_val_if_fail (USAGE_IS_APP_INSTANCE (self), FALSE);

  return USAGE_APP_INSTANCE_GET_CLASS (self)->force_quit_finish (self, result, error);
}

gboolean
usage_app_instance_equal (UsageAppInstance *self,
                          UsageAppInstance *other)
{
  g_return_val_if_fail (USAGE_IS_APP_INSTANCE (self), FALSE);
  g_return_val_if_fail (USAGE_IS_APP_INSTANCE (other), FALSE);

  return g_strcmp0 (usage_app_instance_get_id (self),
                    usage_app_instance_get_id (other)) == 0;
}

gint
usage_app_instance_compare (UsageAppInstance *a,
                            UsageAppInstance *b)
{
  const gchar *a_app_id = usage_app_instance_get_app_id (a);
  const gchar *b_app_id = usage_app_instance_get_app_id (b);

  if (a_app_id == NULL)
    a_app_id = usage_app_instance_get_id (a);

  if (b_app_id == NULL)
    b_app_id = usage_app_instance_get_id (b);

  return g_strcmp0 (a_app_id, b_app_id);
}
