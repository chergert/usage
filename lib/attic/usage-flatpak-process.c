/* usage-flatpak-process.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "usage-flatpak-process.h"

struct _UsageFlatpakProcess
{
  GObject  parent_instance;
  GPid     pid;
  gchar   *instance;
  gchar   *app_id;
  gchar   *branch;
  gchar   *runtime;
  gchar   *runtime_branch;
};

static void
process_iface_init (UsageProcessInterface *iface)
{
}

G_DEFINE_TYPE_WITH_CODE (UsageFlatpakProcess, usage_flatpak_process, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (USAGE_TYPE_PROCESS, process_iface_init))

enum {
  PROP_0,
  PROP_APP_ID,
  PROP_INSTANCE,
  PROP_BRANCH,
  PROP_RUNTIME,
  PROP_RUNTIME_BRANCH,
  PROP_PID,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

UsageFlatpakProcess *
usage_flatpak_process_new (const gchar *instance)
{
  return g_object_new (USAGE_TYPE_FLATPAK_PROCESS,
                       "instance", instance,
                       NULL);
}

static void
usage_flatpak_process_finalize (GObject *object)
{
  UsageFlatpakProcess *self = (UsageFlatpakProcess *)object;

  G_OBJECT_CLASS (usage_flatpak_process_parent_class)->finalize (object);
}

static void
usage_flatpak_process_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  UsageFlatpakProcess *self = USAGE_FLATPAK_PROCESS (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
usage_flatpak_process_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  UsageFlatpakProcess *self = USAGE_FLATPAK_PROCESS (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
usage_flatpak_process_class_init (UsageFlatpakProcessClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = usage_flatpak_process_finalize;
  object_class->get_property = usage_flatpak_process_get_property;
  object_class->set_property = usage_flatpak_process_set_property;
}

static void
usage_flatpak_process_init (UsageFlatpakProcess *self)
{
}
