/* usage-process-provider.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "usage-process.h"

G_BEGIN_DECLS

#define USAGE_TYPE_PROCESS_PROVIDER (usage_process_provider_get_type ())

G_DECLARE_INTERFACE (UsageProcessProvider, usage_process_provider, USAGE, PROCESS_PROVIDER, GObject)

struct _UsageProcessProviderInterface
{
  GTypeInterface parent;

  /* Virtual Functions */
  void     (*load_async)  (UsageProcessProvider *self,
                           GCancellable         *cancellable,
                           GAsyncReadyCallback   callback,
                           gpointer              user_data);
  gboolean (*load_finish) (UsageProcessProvider *self,
                           GAsyncResult         *result,
                           GError              **error);
  void     (*unload)      (UsageProcessProvider *self);

  /* Signals */
  void (*process_added)   (UsageProcessProvider *self,
                           UsageProcess         *process);
  void (*process_removed) (UsageProcessProvider *self,
                           UsageProcess         *process);
};

void     usage_process_provider_load_async           (UsageProcessProvider  *self,
                                                      GCancellable          *cancellable,
                                                      GAsyncReadyCallback    callback,
                                                      gpointer               user_data);
gboolean usage_process_provider_load_finish          (UsageProcessProvider  *self,
                                                      GAsyncResult          *result,
                                                      GError               **error);
void     usage_process_provider_unload               (UsageProcessProvider  *self);
void     usage_process_provider_emit_process_added   (UsageProcessProvider  *self,
                                                      UsageProcess          *process);
void     usage_process_provider_emit_process_removed (UsageProcessProvider  *self,
                                                      UsageProcess          *process);

G_END_DECLS
