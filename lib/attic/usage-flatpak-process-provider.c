/* usage-flatpak-process-provider.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "usage-flatpak-process-provider.h"

struct _UsageFlatpakProcessProvider
{
  GObject       parent_instance;
  GSequence    *processes;
  GCancellable *cancellable;
};

static void
usage_flatpak_process_provider_load_async (UsageFlatpakProcessProvider *self,
                                           GCancellable                *cancellable,
                                           GAsyncReadyCallback          callback,
                                           gpointer                     user_data)
{
  g_autoptr(GTask) task = NULL;

  g_assert (USAGE_IS_FLATPAK_PROCESS_PROVIDER (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, usage_flatpak_process_provider_load_async);


}

static gboolean
usage_flatpak_process_provider_load_finish (UsageFlatpakProcessProvider  *self,
                                            GAsyncResult                 *result,
                                            GError                      **error)
{
  g_assert (USAGE_IS_FLATPAK_PROCESS_PROVIDER (self));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
usage_flatpak_process_provider_unload (UsageProcessProvider *provider)
{
  UsageFlatpakProcessProvider *self = (UsageFlatpakProcessProvider *)provider;

  g_assert (USAGE_IS_FLATPAK_PROCESS_PROVIDER (self));

  g_cancellable_cancel (self->cancellable);
}

static void
process_provider_iface_init (UsageFlatpakProcessProviderInterface *iface)
{
  iface->load_async = usage_flatpak_process_provider_load_async;
  iface->load_finish = usage_flatpak_process_provider_load_finish;
  iface->unload = usage_flatpak_process_provider_unload;
}

G_DEFINE_TYPE (UsageFlatpakProcessProvider, usage_flatpak_process_provider, G_TYPE_OBJECT,
               G_IMPLEMENT_INTERFACE (USAGE_TYPE_PROCESS_PROVIDER, process_provider_iface_init))

UsageFlatpakProcessProvider *
usage_flatpak_process_provider_new (void)
{
  return g_object_new (USAGE_TYPE_FLATPAK_PROCESS_PROVIDER, NULL);
}

static void
usage_flatpak_process_provider_dispose (GObject *object)
{
  UsageFlatpakProcessProvider *self = (UsageFlatpakProcessProvider *)object;

  g_clear_pointer (&self->processes, g_sequence_free);

  G_OBJECT_CLASS (usage_flatpak_process_provider_parent_class)->dispose (object);
}

static void
usage_flatpak_process_provider_class_init (UsageFlatpakProcessProviderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = usage_flatpak_process_provider_dispose;
}

static void
usage_flatpak_process_provider_init (UsageFlatpakProcessProvider *self)
{
  self->processes = g_sequence_new (g_object_unref);
}
