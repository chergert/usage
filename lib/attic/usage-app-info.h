/* usage-app-info.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define USAGE_TYPE_APP_INFO (usage_app_info_get_type())

G_DECLARE_DERIVABLE_TYPE (UsageAppInfo, usage_app_info, USAGE, APP_INFO, GObject)

struct _UsageAppInfoClass
{
  GObjectClass parent_class;
};

UsageAppInfo *usage_app_info_new      (const gchar  *app_id);
const gchar  *usage_app_info_get_id   (UsageAppInfo *self);
const gchar  *usage_app_info_get_name (UsageAppInfo *self);
void          usage_app_info_set_name (UsageAppInfo *self,
                                       const gchar  *name);

G_END_DECLS
