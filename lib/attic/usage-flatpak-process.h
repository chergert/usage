/* usage-flatpak-process.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "usage-process.h"

G_BEGIN_DECLS

#define USAGE_TYPE_FLATPAK_PROCESS (usage_flatpak_process_get_type())

G_DECLARE_FINAL_TYPE (UsageFlatpakProcess, usage_flatpak_process, USAGE, FLATPAK_PROCESS, GObject)

UsageFlatpakProcess *usage_flatpak_process_new                (const gchar *instance);
const gchar         *usage_flatpak_process_get_instance       (UsageFlatpakProcess *self);
const gchar         *usage_flatpak_process_get_branch         (UsageFlatpakProcess *self);
void                 usage_flatpak_process_set_branch         (UsageFlatpakProcess *self,
                                                               const gchar         *branch);
const gchar         *usage_flatpak_process_get_runtime        (UsageFlatpakProcess *self);
void                 usage_flatpak_process_set_runtime        (UsageFlatpakProcess *self,
                                                               const gchar         *runtime);
const gchar         *usage_flatpak_process_get_runtime_branch (UsageFlatpakProcess *self);
void                 usage_flatpak_process_set_runtime_branch (UsageFlatpakProcess *self);

G_END_DECLS
