/* usage-app-info.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "usage-app-info.h"

typedef struct
{
  gchar *id;
  gchar *icon_name;
  gchar *name;
} UsageAppInfoPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (UsageAppInfo, usage_app_info, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_ICON_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

UsageAppInfo *
usage_app_info_new (const gchar *id)
{
  return g_object_new (USAGE_TYPE_APP_INFO,
                       "id", id,
                       NULL);
}

static void
usage_app_info_finalize (GObject *object)
{
  UsageAppInfo *self = (UsageAppInfo *)object;
  UsageAppInfoPrivate *priv = usage_app_info_get_instance_private (self);

  g_clear_pointer (&priv->id, g_free);
  g_clear_pointer (&priv->icon_name, g_free);
  g_clear_pointer (&priv->name, g_free);

  G_OBJECT_CLASS (usage_app_info_parent_class)->finalize (object);
}

static void
usage_app_info_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  UsageAppInfo *self = USAGE_APP_INFO (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, usage_app_info_get_id (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, usage_app_info_get_name (self));
      break;

    case PROP_ICON_NAME:
      g_value_set_string (value, usage_app_info_get_icon_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
usage_app_info_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  UsageAppInfo *self = USAGE_APP_INFO (object);
  UsageAppInfoPrivate *priv = usage_app_info_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ID:
      priv->id = g_value_dup_string (value);
      break;

    case PROP_NAME:
      usage_app_info_set_name (self, g_value_get_string (value));
      break;

    case PROP_ICON_NAME:
      usage_app_info_set_icon_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
usage_app_info_class_init (UsageAppInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = usage_app_info_finalize;
  object_class->get_property = usage_app_info_get_property;
  object_class->set_property = usage_app_info_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "The identifier of the ID",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the application",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
usage_app_info_init (UsageAppInfo *self)
{
}

const gchar *
usage_app_info_get_id (UsageAppInfo *self)
{
  UsageAppInfoPrivate *priv = usage_app_info_get_instance_private (self);

  g_return_val_if_fail (USAGE_IS_APP_INFO (self), NULL);

  return priv->id;
}

const gchar *
usage_app_info_get_name (UsageAppInfo *self)
{
  UsageAppInfoPrivate *priv = usage_app_info_get_instance_private (self);

  g_return_val_if_fail (USAGE_IS_APP_INFO (self), NULL);

  return priv->name;
}

void
usage_app_info_set_name (UsageAppInfo *self,
                         const gchar  *name)
{
  UsageAppInfoPrivate *priv = usage_app_info_get_instance_private (self);

  g_return_if_fail (USAGE_IS_APP_INFO (self));

  if (g_strcmp0 (name, priv->name) != 0)
    {
      g_free (priv->name);
      priv->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

const gchar *
usage_app_info_get_icon_name (UsageAppInfo *self)
{
  UsageAppInfoPrivate *priv = usage_app_info_get_instance_private (self);

  g_return_val_if_fail (USAGE_IS_APP_INFO (self), NULL);

  return priv->icon_name;
}

void
usage_app_info_set_icon_name (UsageAppInfo *self,
                              const gchar  *icon_name)
{
  UsageAppInfoPrivate *priv = usage_app_info_get_instance_private (self);

  g_return_if_fail (USAGE_IS_APP_INFO (self));

  if (g_strcmp0 (icon_name, priv->icon_name) != 0)
    {
      g_free (priv->icon_name);
      priv->icon_name = g_strdup (icon_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ICON_NAME]);
    }
}
