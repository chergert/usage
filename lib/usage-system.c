/* usage-system.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "usage-system.h"

#ifdef __linux__
# include "usage-linux-system.h"
#endif

typedef struct
{
  GPtrArray  *app_instance_providers;
  GListStore *app_instances;
} UsageSystemPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (UsageSystem, usage_system, G_TYPE_OBJECT)

static void
usage_system_finalize (GObject *object)
{
  UsageSystem *self = (UsageSystem *)object;
  UsageSystemPrivate *priv = usage_system_get_instance_private (self);

  g_clear_pointer (&priv->app_instance_providers, g_ptr_array_unref);
  g_clear_object (&priv->app_instances);

  G_OBJECT_CLASS (usage_system_parent_class)->finalize (object);
}

static void
usage_system_class_init (UsageSystemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = usage_system_finalize;
}

static void
usage_system_init (UsageSystem *self)
{
  UsageSystemPrivate *priv = usage_system_get_instance_private (self);

  priv->app_instance_providers = g_ptr_array_new_with_free_func (g_object_unref);
  priv->app_instances = g_list_store_new (USAGE_TYPE_APP_INSTANCE);
}

gboolean
usage_system_poll_info (UsageSystem      *self,
                        UsageSystemInfo  *info,
                        GError          **error)
{
  g_return_val_if_fail (USAGE_IS_SYSTEM (self), FALSE);
  g_return_val_if_fail (info != NULL, FALSE);

  return USAGE_SYSTEM_GET_CLASS (self)->poll_info (self, info, error);
}

static void
usage_system_app_instance_added_cb (UsageSystem              *self,
                                    UsageAppInstance         *instance,
                                    UsageAppInstanceProvider *provider)
{
  UsageSystemPrivate *priv = usage_system_get_instance_private (self);

  g_assert (USAGE_IS_SYSTEM (self));
  g_assert (USAGE_IS_APP_INSTANCE (instance));
  g_assert (USAGE_IS_APP_INSTANCE_PROVIDER (provider));

  if (priv->app_instances != NULL)
    g_list_store_insert_sorted (priv->app_instances,
                                instance,
                                (GCompareDataFunc)usage_app_instance_compare,
                                NULL);
}

static void
usage_system_app_instance_removed_cb (UsageSystem              *self,
                                      UsageAppInstance         *instance,
                                      UsageAppInstanceProvider *provider)
{
  UsageSystemPrivate *priv = usage_system_get_instance_private (self);

  g_assert (USAGE_IS_SYSTEM (self));
  g_assert (USAGE_IS_APP_INSTANCE (instance));
  g_assert (USAGE_IS_APP_INSTANCE_PROVIDER (provider));

  if (priv->app_instances != NULL)
    {
      guint n_items = g_list_model_get_n_items (G_LIST_MODEL (priv->app_instances));

      for (guint i = 0; i < n_items; i++)
        {
          g_autoptr(UsageAppInstance) ele = NULL;

          ele = g_list_model_get_item (G_LIST_MODEL (priv->app_instances), i);

          if (usage_app_instance_equal (ele, instance))
            {
              g_list_store_remove (priv->app_instances, i);
              break;
            }
        }
    }
}

void
usage_system_add_app_instance_provider (UsageSystem              *self,
                                        UsageAppInstanceProvider *app_instance_provider)
{
  UsageSystemPrivate *priv = usage_system_get_instance_private (self);

  g_return_if_fail (USAGE_IS_SYSTEM (self));
  g_return_if_fail (USAGE_IS_APP_INSTANCE_PROVIDER (app_instance_provider));

  if (!g_ptr_array_find (priv->app_instance_providers, app_instance_provider, NULL))
    {
      g_ptr_array_add (priv->app_instance_providers, g_object_ref (app_instance_provider));
      g_signal_connect_object (app_instance_provider,
                               "instance-added",
                               G_CALLBACK (usage_system_app_instance_added_cb),
                               self,
                               G_CONNECT_SWAPPED);
      g_signal_connect_object (app_instance_provider,
                               "instance-removed",
                               G_CALLBACK (usage_system_app_instance_removed_cb),
                               self,
                               G_CONNECT_SWAPPED);
    }
}

/**
 * usage_system_get_app_instances:
 * @self: an #UsageSystem
 *
 * Gets a #GListModel containing the available app instances.
 *
 * Returns: (transfer none): a #GListModel
 */
GListModel *
usage_system_get_app_instances (UsageSystem *self)
{
  UsageSystemPrivate *priv = usage_system_get_instance_private (self);

  g_return_val_if_fail (USAGE_IS_SYSTEM (self), NULL);

  return G_LIST_MODEL (priv->app_instances);
}

/**
 * usage_system_list_pids:
 * @self: a #UsageSystem
 *
 * Gets a list of #GPid
 *
 * Returns: (transfer full) (element-type GPid): a #GArray of #GPid
 */
GArray *
usage_system_list_pids (UsageSystem  *self,
                        GError      **error)
{
  g_return_val_if_fail (USAGE_IS_SYSTEM (self), NULL);

  return USAGE_SYSTEM_GET_CLASS (self)->list_pids (self, error);
}
