/* usage-app-instance.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define USAGE_TYPE_APP_INSTANCE (usage_app_instance_get_type())

G_DECLARE_DERIVABLE_TYPE (UsageAppInstance, usage_app_instance, USAGE, APP_INSTANCE, GObject)

struct _UsageAppInstanceClass
{
  GObjectClass parent_class;

  void     (*force_quit_async)  (UsageAppInstance     *self,
                                 GCancellable         *cancellable,
                                 GAsyncReadyCallback   callback,
                                 gpointer              user_data);
  gboolean (*force_quit_finish) (UsageAppInstance     *self,
                                 GAsyncResult         *result,
                                 GError              **error);
};

UsageAppInstance *usage_app_instance_new               (const gchar          *id);
gint              usage_app_instance_compare           (UsageAppInstance     *a,
                                                        UsageAppInstance     *b);
gboolean          usage_app_instance_equal             (UsageAppInstance     *self,
                                                        UsageAppInstance     *other);
const gchar      *usage_app_instance_get_id            (UsageAppInstance     *self);
const gchar      *usage_app_instance_get_app_id        (UsageAppInstance     *self);
void              usage_app_instance_set_app_id        (UsageAppInstance     *self,
                                                        const gchar          *app_id);
GPid              usage_app_instance_get_pid           (UsageAppInstance     *self);
void              usage_app_instance_set_pid           (UsageAppInstance     *self,
                                                        GPid                  pid);
void              usage_app_instance_force_quit_async  (UsageAppInstance     *self,
                                                        GCancellable         *cancellable,
                                                        GAsyncReadyCallback   callback,
                                                        gpointer              user_data);
gboolean          usage_app_instance_force_quit_finish (UsageAppInstance     *self,
                                                        GAsyncResult         *result,
                                                        GError              **error);

G_END_DECLS
