/* usage-flatpak-app-instance-provider.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include "usage-flatpak-app-instance.h"
#include "usage-flatpak-app-instance-provider.h"
#include "usage-line-reader.h"
#include "usage-util.h"

#define QUEUED_UPDATE_DELAY_SEC 1

struct _UsageFlatpakAppInstanceProvider
{
  GObject parent_instance;
  GPtrArray *active;
  guint update_source;
};

static void usage_flatpak_app_instance_provider_queue_update (UsageFlatpakAppInstanceProvider *self);

G_DEFINE_TYPE_WITH_CODE (UsageFlatpakAppInstanceProvider, usage_flatpak_app_instance_provider, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (USAGE_TYPE_APP_INSTANCE_PROVIDER, NULL))

UsageAppInstanceProvider *
usage_flatpak_app_instance_provider_new (void)
{
  return g_object_new (USAGE_TYPE_FLATPAK_APP_INSTANCE_PROVIDER, NULL);
}

static gboolean
contains_instance (GPtrArray   *ar,
                   const gchar *id)
{
  if (ar == NULL || id == NULL)
    return FALSE;

  for (guint i = 0; i < ar->len; i++)
    {
      UsageAppInstance *instance = g_ptr_array_index (ar, i);
      const gchar *instance_id = usage_app_instance_get_id (instance);

      if (g_strcmp0 (instance_id, id) == 0)
        return TRUE;
    }

  return FALSE;
}

static void
usage_flatpak_app_instance_provider_poll_cb (GObject      *object,
                                             GAsyncResult *result,
                                             gpointer      user_data)
{
  GSubprocess *subprocess = (GSubprocess *)object;
  g_autoptr(UsageFlatpakAppInstanceProvider) self = user_data;
  g_autoptr(GPtrArray) found = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *stdout_buf = NULL;
  UsageLineReader reader;
  gchar *line;
  gsize line_len;

  g_assert (G_IS_SUBPROCESS (subprocess));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (USAGE_IS_FLATPAK_APP_INSTANCE_PROVIDER (self));

  if (!g_subprocess_communicate_utf8_finish (subprocess, result, &stdout_buf, NULL, &error))
    {
      g_warning ("Failed to read reply from 'flatpak ps': %s", error->message);
      return;
    }

  found = g_ptr_array_new_with_free_func (g_object_unref);

  usage_line_reader_init (&reader, stdout_buf, -1);

  /* Now process instance lines from "flatpak ps", no header is
   * provided when running without a PTY.
   */
  while ((line = usage_line_reader_next (&reader, &line_len)))
    {
      g_autoptr(UsageAppInstance) instance = NULL;
      gchar *bufptr = line;
      gchar *save = NULL;
      gchar *word;
      GPid pid;

      line[line_len] = 0;

      /* Get the instance id, then clear bufptr for future strtok calls */
      if (!(word = strtok_r (bufptr, " \t", &save)))
        break;

      bufptr = NULL;

      instance = usage_flatpak_app_instance_new (word);

      /* Now get the PID as a string */
      if (!(word = strtok_r (bufptr, " \t", &save)) || !(pid = atoi (word)))
        continue;
      else
        usage_app_instance_set_pid (instance, pid);

      /* Now get the app id */
      if (!(word = strtok_r (bufptr, " \t", &save)))
        continue;
      else
        usage_app_instance_set_app_id (instance, word);

      /* Now get the apps runtime */
      if (!(word = strtok_r (bufptr, " \t", &save)))
        continue;
      else
        usage_flatpak_app_instance_set_runtime (USAGE_FLATPAK_APP_INSTANCE (instance), word);

      /* Now get the runtime branch */
      if (!(word = strtok_r (bufptr, " \t", &save)))
        continue;
      else
        usage_flatpak_app_instance_set_runtime_branch (USAGE_FLATPAK_APP_INSTANCE (instance), word);

      /* Now get the app branch. We do this last because flatpak ps might omit it
       * if we are running out of a "flatpak build" environment.
       */
      if ((word = strtok_r (bufptr, " \t", &save)))
        usage_flatpak_app_instance_set_branch (USAGE_FLATPAK_APP_INSTANCE (instance), word);

      g_ptr_array_add (found, g_steal_pointer (&instance));
    }

  if (self->active != NULL)
    {
      for (guint i = 0; i < self->active->len; i++)
        {
          UsageAppInstance *instance = g_ptr_array_index (self->active, i);
          const gchar *id = usage_app_instance_get_id (instance);

          if (!contains_instance (found, id))
            usage_app_instance_provider_instance_removed (USAGE_APP_INSTANCE_PROVIDER (self), instance);
        }
    }

  for (guint i = 0; i < found->len; i++)
    {
      UsageAppInstance *instance = g_ptr_array_index (found, i);
      const gchar *id = usage_app_instance_get_id (instance);

      if (self->active == NULL || !contains_instance (self->active, id))
        usage_app_instance_provider_instance_added (USAGE_APP_INSTANCE_PROVIDER (self), instance);
    }

  g_clear_pointer (&self->active, g_ptr_array_unref);
  self->active = g_steal_pointer (&found);

  usage_flatpak_app_instance_provider_queue_update (self);
}

static gboolean
usage_flatpak_app_instance_provider_poll (gpointer data)
{
  UsageFlatpakAppInstanceProvider *self = data;
  g_autoptr(GSubprocessLauncher) launcher = NULL;
  g_autoptr(GSubprocess) subprocess = NULL;
  g_autoptr(GError) error = NULL;
  g_auto(GStrv) args = NULL;

  g_assert (USAGE_IS_FLATPAK_APP_INSTANCE_PROVIDER (self));

  self->update_source = 0;

  args = usage_util_build_argv ("flatpak",
                                "ps",
                                "--columns=instance,pid,application,runtime,runtime-branch,branch",
                                NULL);

  launcher = g_subprocess_launcher_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE | G_SUBPROCESS_FLAGS_STDERR_SILENCE);
  g_subprocess_launcher_set_cwd (launcher, g_get_home_dir ());
  subprocess = g_subprocess_launcher_spawnv (launcher, (const gchar * const *)args, &error);

  if (subprocess == NULL)
    g_warning ("Failed to spawn 'flatpak ps': %s", error->message);
  else
    g_subprocess_communicate_utf8_async (subprocess,
                                         NULL,
                                         NULL,
                                         usage_flatpak_app_instance_provider_poll_cb,
                                         g_object_ref (self));

  return G_SOURCE_REMOVE;
}

static void
usage_flatpak_app_instance_provider_queue_update (UsageFlatpakAppInstanceProvider *self)
{
  g_assert (USAGE_IS_FLATPAK_APP_INSTANCE_PROVIDER (self));

  if (self->update_source == 0)
    self->update_source = g_timeout_add_seconds_full (G_PRIORITY_DEFAULT,
                                                      QUEUED_UPDATE_DELAY_SEC,
                                                      usage_flatpak_app_instance_provider_poll,
                                                      g_object_ref (self),
                                                      g_object_unref);
}

static void
usage_flatpak_app_instance_provider_constructed (GObject *object)
{
  UsageFlatpakAppInstanceProvider *self = (UsageFlatpakAppInstanceProvider *)object;

  G_OBJECT_CLASS (usage_flatpak_app_instance_provider_parent_class)->constructed (object);

  usage_flatpak_app_instance_provider_queue_update (self);
}

static void
usage_flatpak_app_instance_provider_finalize (GObject *object)
{
  UsageFlatpakAppInstanceProvider *self = (UsageFlatpakAppInstanceProvider *)object;

  g_clear_pointer (&self->active, g_ptr_array_unref);

  G_OBJECT_CLASS (usage_flatpak_app_instance_provider_parent_class)->finalize (object);
}

static void
usage_flatpak_app_instance_provider_class_init (UsageFlatpakAppInstanceProviderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = usage_flatpak_app_instance_provider_constructed;
  object_class->finalize = usage_flatpak_app_instance_provider_finalize;
}

static void
usage_flatpak_app_instance_provider_init (UsageFlatpakAppInstanceProvider *self)
{
}
