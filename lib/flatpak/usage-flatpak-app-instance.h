/* usage-flatpak-app-instance.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "usage-app-instance.h"

G_BEGIN_DECLS

#define USAGE_TYPE_FLATPAK_APP_INSTANCE (usage_flatpak_app_instance_get_type())

G_DECLARE_FINAL_TYPE (UsageFlatpakAppInstance, usage_flatpak_app_instance, USAGE, FLATPAK_APP_INSTANCE, UsageAppInstance)

UsageAppInstance *usage_flatpak_app_instance_new                (const gchar             *id);
const gchar      *usage_flatpak_app_instance_get_branch         (UsageFlatpakAppInstance *self);
void              usage_flatpak_app_instance_set_branch         (UsageFlatpakAppInstance *self,
                                                                 const gchar             *branch);
const gchar      *usage_flatpak_app_instance_get_runtime        (UsageFlatpakAppInstance *self);
void              usage_flatpak_app_instance_set_runtime        (UsageFlatpakAppInstance *self,
                                                                 const gchar             *runtime);
const gchar      *usage_flatpak_app_instance_get_runtime_branch (UsageFlatpakAppInstance *self);
void              usage_flatpak_app_instance_set_runtime_branch (UsageFlatpakAppInstance *self,
                                                                 const gchar             *runtime_branch);

G_END_DECLS
