/* usage-flatpak-app-instance.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "usage-flatpak-app-instance.h"
#include "usage-util.h"

struct _UsageFlatpakAppInstance
{
  UsageAppInstance parent_instance;
  gchar *branch;
  gchar *runtime;
  gchar *runtime_branch;
};

G_DEFINE_TYPE (UsageFlatpakAppInstance, usage_flatpak_app_instance, USAGE_TYPE_APP_INSTANCE)

enum {
  PROP_0,
  PROP_BRANCH,
  PROP_RUNTIME,
  PROP_RUNTIME_BRANCH,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

UsageAppInstance *
usage_flatpak_app_instance_new (const gchar *id)
{
  return g_object_new (USAGE_TYPE_FLATPAK_APP_INSTANCE,
                       "id", id,
                       NULL);
}

static void
usage_flatpak_app_instance_force_quit_cb (GObject      *object,
                                          GAsyncResult *result,
                                          gpointer      user_data)
{
  GSubprocess *subprocess = (GSubprocess *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  g_assert (G_IS_SUBPROCESS (subprocess));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!g_subprocess_wait_check_finish (subprocess, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

static void
usage_flatpak_app_instance_force_quit_async (UsageAppInstance    *app_instance,
                                             GCancellable        *cancellable,
                                             GAsyncReadyCallback  callback,
                                             gpointer             user_data)
{
  UsageFlatpakAppInstance *self = (UsageFlatpakAppInstance *)app_instance;
  g_autoptr(GSubprocessLauncher) launcher = NULL;
  g_autoptr(GSubprocess) subprocess = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = NULL;
  g_auto(GStrv) args = NULL;
  const gchar *id;

  g_assert (USAGE_IS_FLATPAK_APP_INSTANCE (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, usage_flatpak_app_instance_force_quit_async);

  id = usage_app_instance_get_id (app_instance);
  args = usage_util_build_argv ("flatpak", "kill", id, NULL);

  launcher = g_subprocess_launcher_new (G_SUBPROCESS_FLAGS_STDOUT_SILENCE |
                                        G_SUBPROCESS_FLAGS_STDERR_SILENCE);
  subprocess = g_subprocess_launcher_spawnv (launcher, (const gchar * const *)args, &error);

  if (subprocess == NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_subprocess_wait_check_async (subprocess,
                                   cancellable,
                                   usage_flatpak_app_instance_force_quit_cb,
                                   g_steal_pointer (&task));
}

static gboolean
usage_flatpak_app_instance_force_quit_finish (UsageAppInstance  *self,
                                              GAsyncResult      *result,
                                              GError           **error)
{
  g_return_val_if_fail (USAGE_IS_FLATPAK_APP_INSTANCE (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
usage_flatpak_app_instance_finalize (GObject *object)
{
  UsageFlatpakAppInstance *self = (UsageFlatpakAppInstance *)object;

  g_clear_pointer (&self->branch, g_free);
  g_clear_pointer (&self->runtime, g_free);
  g_clear_pointer (&self->runtime_branch, g_free);

  G_OBJECT_CLASS (usage_flatpak_app_instance_parent_class)->finalize (object);
}

static void
usage_flatpak_app_instance_get_property (GObject    *object,
                                         guint       prop_id,
                                         GValue     *value,
                                         GParamSpec *pspec)
{
  UsageFlatpakAppInstance *self = USAGE_FLATPAK_APP_INSTANCE (object);

  switch (prop_id)
    {
    case PROP_BRANCH:
      g_value_set_string (value, usage_flatpak_app_instance_get_branch (self));
      break;

    case PROP_RUNTIME:
      g_value_set_string (value, usage_flatpak_app_instance_get_runtime (self));
      break;

    case PROP_RUNTIME_BRANCH:
      g_value_set_string (value, usage_flatpak_app_instance_get_runtime_branch (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
usage_flatpak_app_instance_set_property (GObject      *object,
                                         guint         prop_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  UsageFlatpakAppInstance *self = USAGE_FLATPAK_APP_INSTANCE (object);

  switch (prop_id)
    {
    case PROP_BRANCH:
      usage_flatpak_app_instance_set_branch (self, g_value_get_string (value));
      break;

    case PROP_RUNTIME:
      usage_flatpak_app_instance_set_runtime (self, g_value_get_string (value));
      break;

    case PROP_RUNTIME_BRANCH:
      usage_flatpak_app_instance_set_runtime_branch (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
usage_flatpak_app_instance_class_init (UsageFlatpakAppInstanceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  UsageAppInstanceClass *app_instance_class = USAGE_APP_INSTANCE_CLASS (klass);

  object_class->finalize = usage_flatpak_app_instance_finalize;
  object_class->get_property = usage_flatpak_app_instance_get_property;
  object_class->set_property = usage_flatpak_app_instance_set_property;

  app_instance_class->force_quit_async = usage_flatpak_app_instance_force_quit_async;
  app_instance_class->force_quit_finish = usage_flatpak_app_instance_force_quit_finish;

  properties [PROP_BRANCH] =
    g_param_spec_string ("branch",
                         "Branch",
                         "The branch of the flatpak application",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_RUNTIME] =
    g_param_spec_string ("runtime",
                         "Runtime",
                         "The runtime used by the flatpak application",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_RUNTIME_BRANCH] =
    g_param_spec_string ("runtime-branch",
                         "Runtime Branch",
                         "The branch of the runtime used by the flatpak application",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
usage_flatpak_app_instance_init (UsageFlatpakAppInstance *self)
{
}

const gchar *
usage_flatpak_app_instance_get_branch (UsageFlatpakAppInstance *self)
{
  g_return_val_if_fail (USAGE_IS_FLATPAK_APP_INSTANCE (self), NULL);

  return self->branch;
}

void
usage_flatpak_app_instance_set_branch (UsageFlatpakAppInstance *self,
                                       const gchar             *branch)
{
  g_return_if_fail (USAGE_IS_FLATPAK_APP_INSTANCE (self));

  if (g_strcmp0 (self->branch, branch) != 0)
    {
      g_free (self->branch);
      self->branch = g_strdup (branch);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BRANCH]);
    }
}

const gchar *
usage_flatpak_app_instance_get_runtime (UsageFlatpakAppInstance *self)
{
  g_return_val_if_fail (USAGE_IS_FLATPAK_APP_INSTANCE (self), NULL);

  return self->runtime;
}

void
usage_flatpak_app_instance_set_runtime (UsageFlatpakAppInstance *self,
                                        const gchar             *runtime)
{
  g_return_if_fail (USAGE_IS_FLATPAK_APP_INSTANCE (self));

  if (g_strcmp0 (self->runtime, runtime) != 0)
    {
      g_free (self->runtime);
      self->runtime = g_strdup (runtime);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_RUNTIME]);
    }
}

const gchar *
usage_flatpak_app_instance_get_runtime_branch (UsageFlatpakAppInstance *self)
{
  g_return_val_if_fail (USAGE_IS_FLATPAK_APP_INSTANCE (self), NULL);

  return self->runtime_branch;
}

void
usage_flatpak_app_instance_set_runtime_branch (UsageFlatpakAppInstance *self,
                                               const gchar             *runtime_branch)
{
  g_return_if_fail (USAGE_IS_FLATPAK_APP_INSTANCE (self));

  if (g_strcmp0 (self->runtime_branch, runtime_branch) != 0)
    {
      g_free (self->runtime_branch);
      self->runtime_branch = g_strdup (runtime_branch);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_RUNTIME_BRANCH]);
    }
}
