/* usage-app-info-provider.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "usage-app-info-provider.h"

G_DEFINE_INTERFACE (UsageAppInfoProvider, usage_app_info_provider, G_TYPE_OBJECT)

static void
usage_app_info_provider_default_init (UsageAppInfoProviderInterface *iface)
{
}

/**
 * usage_app_info_provider_find_by_id:
 *
 * Returns: (transfer full) (nullable): an #UsageAppInfoProvider
 */
UsageAppInfo *
usage_app_info_provider_find_by_id (UsageAppInfoProvider *self,
                                    const gchar          *id)
{
  g_return_val_if_fail (USAGE_IS_APP_INFO_PROVIDER (self), NULL);
  g_return_val_if_fail (id != NULL, NULL);

  if (USAGE_APP_INFO_PROVIDER_GET_IFACE (self)->find_by_id)
    return USAGE_APP_INFO_PROVIDER_GET_IFACE (self)->find_by_id (self, id);

  return NULL;
}
