/* usage-storage-provider.h
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "usage-storage-device.h"

G_BEGIN_DECLS

#define USAGE_TYPE_STORAGE_PROVIDER (usage_storage_provider_get_type())

G_DECLARE_INTERFACE (UsageStorageProvider, usage_storage_provider, USAGE, STORAGE_PROVIDER, GObject)

struct _UsageStorageProviderInterface
{
  GTypeInterface parent_iface;

  void        (*list_disks_async)  (UsageStorageProvider  *self,
                                    GCancellable          *cancellable,
                                    GAsyncReadyCallback    callback,
                                    gpointer               user_data);
  GListModel *(*list_disks_finish) (UsageStorageProvider  *self,
                                    GAsyncResult          *result,
                                    GError               **error);
};

void        usage_storage_provider_list_disks_async  (UsageStorageProvider  *self,
                                                      GCancellable          *cancellable,
                                                      GAsyncReadyCallback    callback,
                                                      gpointer               user_data);
GListModel *usage_storage_provider_list_disks_finish (UsageStorageProvider  *self,
                                                      GAsyncResult          *result,
                                                      GError               **error);

G_END_DECLS
