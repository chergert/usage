/* usage-process-monitor.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define USAGE_TYPE_PROCESS_MONITOR (usage_process_monitor_get_type())

G_DECLARE_FINAL_TYPE (UsageProcessMonitor, usage_process_monitor, USAGE, PROCESS_MONITOR, GObject)

UsageProcessMonitor *usage_process_monitor_new     (void);
void                 usage_process_monitor_hold    (UsageProcessMonitor *self);
void                 usage_process_monitor_release (UsageProcessMonitor *self);
guint usage_process_monitor_get_poll_interval (UsageProcessMonitor *self);
void                 usage_process_monitor_set_poll_interval (UsageProcessMonitor *self,
                                                              guint poll_interval);

G_END_DECLS
