/* usage-linux-system.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "usage-system.h"

G_BEGIN_DECLS

#define USAGE_TYPE_LINUX_SYSTEM (usage_linux_system_get_type())

typedef struct
{
  gchar  device[32];
  gint   major;
  gint   minor;
  gint64 reads_total;
  gint64 reads_merged;
  gint64 reads_sectors;
  gint64 reads_msec;
  gint64 writes_total;
  gint64 writes_merged;
  gint64 writes_sectors;
  gint64 writes_msec;
  gint64 iops_active;
  gint64 iops_msec;
  gint64 iops_msec_weighted;
} UsageLinuxDiskStat;

typedef struct
{
  gchar iface[32];
  guint64 rx_bytes;
  guint64 rx_packets;
  guint64 rx_errors;
  guint64 rx_dropped;
  guint64 rx_fifo;
  guint64 rx_frame;
  guint64 rx_compressed;
  guint64 rx_multicast;
  guint64 tx_bytes;
  guint64 tx_packets;
  guint64 tx_errors;
  guint64 tx_dropped;
  guint64 tx_fifo;
  guint64 tx_collisions;
  guint64 tx_carrier;
  guint64 tx_compressed;
} UsageLinuxNetStat;

G_DECLARE_FINAL_TYPE (UsageLinuxSystem, usage_linux_system, USAGE, LINUX_SYSTEM, UsageSystem)

UsageSystem *usage_linux_system_new           (void);
GArray      *usage_linux_system_poll_diskstat (UsageLinuxSystem *self);
GArray      *usage_linux_system_poll_netstat  (UsageLinuxSystem *self);

G_END_DECLS
