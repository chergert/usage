/* usage-linux-disks-model.c
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "usage-linux-disks-model"

#include "config.h"

#include <udisks/udisks.h>

#include "usage-linux-disks-model.h"
#include "usage-storage-device.h"

struct _UsageLinuxDisksModel
{
  GObject parent_instance;

  UDisksClient *client;

  GPtrArray *items;
};

static GType
usage_linux_disks_model_get_item_type (GListModel *model)
{
  return USAGE_TYPE_STORAGE_DEVICE;
}

static guint
usage_linux_disks_model_get_n_items (GListModel *model)
{
  return USAGE_LINUX_DISKS_MODEL (model)->items->len;
}

static gpointer
usage_linux_disks_model_get_item (GListModel *model,
                                  guint       position)
{
  return g_object_ref (g_ptr_array_index (USAGE_LINUX_DISKS_MODEL (model)->items, position));
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item_type = usage_linux_disks_model_get_item_type;
  iface->get_n_items = usage_linux_disks_model_get_n_items;
  iface->get_item = usage_linux_disks_model_get_item;
}

G_DEFINE_TYPE_WITH_CODE (UsageLinuxDisksModel, usage_linux_disks_model, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

/**
 * usage_linux_disks_model_new:
 *
 * Create a new #UsageLinuxDisksModel.
 *
 * Returns: (transfer full): a newly created #UsageLinuxDisksModel
 */
UsageLinuxDisksModel *
usage_linux_disks_model_new (void)
{
  return g_object_new (USAGE_TYPE_LINUX_DISKS_MODEL, NULL);
}

static void
usage_linux_disks_model_finalize (GObject *object)
{
  UsageLinuxDisksModel *self = (UsageLinuxDisksModel *)object;

  g_clear_pointer (&self->items, g_ptr_array_unref);

  G_OBJECT_CLASS (usage_linux_disks_model_parent_class)->finalize (object);
}

static void
usage_linux_disks_model_class_init (UsageLinuxDisksModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = usage_linux_disks_model_finalize;
}

static void
usage_linux_disks_model_init (UsageLinuxDisksModel *self)
{
  self->items = g_ptr_array_new_with_free_func (g_object_unref);
}

static void
usage_linux_disks_model_changed_cb (UsageLinuxDisksModel *self,
                                    UDisksClient         *client)
{
  g_assert (USAGE_IS_LINUX_DISKS_MODEL (self));
  g_assert (UDISKS_IS_CLIENT (client));

}

gboolean
usage_linux_disks_model_start (UsageLinuxDisksModel  *self,
                               GError               **error)
{
  g_return_val_if_fail (USAGE_IS_LINUX_DISKS_MODEL (self), FALSE);

  if (self->client == NULL)
    {
      if (!(self->client = udisks_client_new_sync (NULL, error)))
        return FALSE;

      /* First register for changes */
      g_signal_connect_object (self->client,
                               "changed",
                               G_CALLBACK (usage_linux_disks_model_changed_cb),
                               self,
                               G_CONNECT_SWAPPED);

      /* Now force any in-flight changes to dispatch */
      udisks_client_settle (self->client);

      /* Follow up in case nothing changed when settling */
      usage_linux_disks_model_changed_cb (self, self->client);
    }

  return TRUE;
}

void
usage_linux_disks_model_stop (UsageLinuxDisksModel *self)
{
  g_return_if_fail (USAGE_IS_LINUX_DISKS_MODEL (self));
}
