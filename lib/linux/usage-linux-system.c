/* usage-linux-system.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define _GNU_SOURCE

#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "usage-flatpak-app-instance-provider.h"
#include "usage-fd-pool-private.h"
#include "usage-line-reader.h"
#include "usage-linux-system.h"

#define ADD_FROM_CHAR(v, c) (((v)*10L)+((c)-'0'))

struct _UsageLinuxSystem
{
  UsageSystem parent_instance;

  GArray *diskstat;
  GArray *netstat;
};

enum {
  /* -2 */ DISKSTAT_MAJOR,
  /* -1 */ DISKSTAT_MINOR,
  /* 0 */  DISKSTAT_NAME,
  /* 1 */  DISKSTAT_READS_TOTAL,
  /* 2 */  DISKSTAT_READS_MERGED,
  /* 3 */  DISKSTAT_READS_SECTORS,
  /* 4 */  DISKSTAT_READS_MSEC,
  /* 5 */  DISKSTAT_WRITES_TOTAL,
  /* 6 */  DISKSTAT_WRITES_MERGED,
  /* 7 */  DISKSTAT_WRITES_SECTORS,
  /* 8 */  DISKSTAT_WRITES_MSEC,
  /* 9 */  DISKSTAT_IOPS_ACTIVE,
  /* 10 */ DISKSTAT_IOPS_MSEC,
  /* 11 */ DISKSTAT_IOPS_MSEC_WEIGHTED,
};

G_DEFINE_TYPE (UsageLinuxSystem, usage_linux_system, USAGE_TYPE_SYSTEM)

UsageSystem *
usage_linux_system_new (void)
{
  return g_object_new (USAGE_TYPE_LINUX_SYSTEM, NULL);
}

static gboolean
get_mem_info (UsageLinuxSystem *self,
              UsageSystemInfo  *info)
{
  static GHashTable *keys = NULL;
  gchar buf[4096];
  gchar *bufptr;
  gchar *save;
  gssize len;
  gint meminfo_fd;
  gboolean ret = FALSE;

  g_assert (USAGE_IS_LINUX_SYSTEM (self));
  g_assert (info != NULL);

  if (keys == NULL)
    {
      keys = g_hash_table_new (g_str_hash, g_str_equal);
#define ADD_OFFSET(n,o) \
  g_hash_table_insert (keys, (gchar *)n, GUINT_TO_POINTER (o))
  ADD_OFFSET ("MemTotal", G_STRUCT_OFFSET (UsageSystemInfo, mem_total));
  ADD_OFFSET ("MemAvailable", G_STRUCT_OFFSET (UsageSystemInfo, mem_available));
  ADD_OFFSET ("Cached", G_STRUCT_OFFSET (UsageSystemInfo, mem_cache));
  ADD_OFFSET ("SwapTotal", G_STRUCT_OFFSET (UsageSystemInfo, swap_total));
  ADD_OFFSET ("SwapFree", G_STRUCT_OFFSET (UsageSystemInfo, swap_free));
#undef ADD_OFFSET
    }

  /* Get FD and seek to beginning to reset it */
  meminfo_fd = usage_fd_pool_get ("/proc/meminfo", O_RDONLY, TRUE);
  if (meminfo_fd == -1)
    return FALSE;

  /* Read head of meminfo, as it contains what we need */
  len = read (meminfo_fd, buf, sizeof buf - 1);
  if (len < 0)
    goto failure;

  g_assert (len < (gssize)sizeof buf);

  buf[len] = 0;
  bufptr = buf;

  for (;;)
    {
      gpointer hval;
      goffset off;
      gchar *key;
      gchar *value;
      gchar *unit;
      gint64 v64;
      gint64 *v64ptr;

      /* Get the data key name */
      if (!(key = strtok_r (bufptr, " \n\t:", &save)))
        break;

      bufptr = NULL;

      /* Offset from self to save value. Stop after getting to
       * last value we care about.
       */
      if (!g_hash_table_lookup_extended (keys, key, NULL, &hval))
        continue;

      off = GPOINTER_TO_SIZE (hval);

      /* Get the data value */
      if (!(value = strtok_r (bufptr, " \n\t:", &save)))
        break;

      /* Parse the numeric value of this column */
      v64 = g_ascii_strtoll (value, NULL, 10);
      if ((v64 == G_MININT64 || v64 == G_MAXINT64) && errno == ERANGE)
        break;

      /* Get the data unit */
      unit = strtok_r (bufptr, " \n\t:", &save);

      if (g_strcmp0 (unit, "kB") == 0)
        v64 *= 1024;
      else if (g_strcmp0 (unit, "mB") == 0)
        v64 *= 1024 * 1024;

      v64ptr = (gint64 *)(gpointer)(((gchar *)info) + off);

      *v64ptr = v64;
    }

  ret = TRUE;

failure:
  usage_fd_pool_put ("/proc/meminfo", meminfo_fd);

  return ret;
}

static const UsageLinuxDiskStat *
find_previous_diskstat (UsageLinuxSystem *self,
                        const gchar      *name)
{
  g_assert (USAGE_IS_LINUX_SYSTEM (self));
  g_assert (name != NULL);

  if (self->diskstat == NULL)
    return NULL;

  for (guint i = 0; i < self->diskstat->len; i++)
    {
      const UsageLinuxDiskStat *ds = &g_array_index (self->diskstat, UsageLinuxDiskStat, i);

      if (strcmp (name, ds->device) == 0)
        return ds;
    }

  return NULL;
}

static gboolean
get_disk_info (UsageLinuxSystem *self,
               UsageSystemInfo  *info)
{
  g_autoptr(GArray) diskstat = NULL;

  g_assert (USAGE_IS_LINUX_SYSTEM (self));
  g_assert (info != NULL);

  if (!(diskstat = usage_linux_system_poll_diskstat (self)))
    return FALSE;

  info->disk_read = 0;
  info->disk_write = 0;

  if (diskstat->len == 0)
    return TRUE;

  for (guint i = 0; i < diskstat->len; i++)
    {
      const UsageLinuxDiskStat *ds = &g_array_index (diskstat, UsageLinuxDiskStat, i);
      const UsageLinuxDiskStat *prev = find_previous_diskstat (self, ds->device);

      if (prev != NULL)
        {
          info->disk_read += ds->reads_sectors - prev->reads_sectors;
          info->disk_write += ds->writes_sectors - prev->writes_sectors;
        }
    }

  g_clear_pointer (&self->diskstat, g_array_unref);
  self->diskstat = g_steal_pointer (&diskstat);

  return TRUE;
}

static gboolean
is_wan (const gchar *str)
{
  /* Don't use systemd predictable device names?
   * Here is a good place to add the alternate names of your drivers.
   */
  return g_str_has_prefix (str, "ww");
}

static gboolean
is_wifi (const gchar *str)
{
  /* Don't use systemd predictable device names?
   * Here is a good place to add the alternate names of your drivers.
   */
  return g_str_has_prefix (str, "wl");
}

static const UsageLinuxNetStat *
find_previous_netstat (UsageLinuxSystem *self,
                        const gchar      *name)
{
  g_assert (USAGE_IS_LINUX_SYSTEM (self));
  g_assert (name != NULL);

  if (self->netstat == NULL)
    return NULL;

  for (guint i = 0; i < self->netstat->len; i++)
    {
      const UsageLinuxNetStat *ns = &g_array_index (self->netstat, UsageLinuxNetStat, i);

      if (strcmp (name, ns->iface) == 0)
        return ns;
    }

  return NULL;
}

static gboolean
get_net_info (UsageLinuxSystem *self,
             UsageSystemInfo  *info)
{
  g_autoptr(GArray) netstat = NULL;

  g_assert (USAGE_IS_LINUX_SYSTEM (self));
  g_assert (info != NULL);

  if (!(netstat = usage_linux_system_poll_netstat (self)))
    return FALSE;

  info->wan_rx = 0;
  info->wan_tx = 0;
  info->lan_rx = 0;
  info->lan_tx = 0;
  info->wifi_rx = 0;
  info->wifi_tx = 0;

  if (netstat->len == 0)
    return TRUE;

  for (guint i = 0; i < netstat->len; i++)
    {
      const UsageLinuxNetStat *ns = &g_array_index (netstat, UsageLinuxNetStat, i);
      const UsageLinuxNetStat *prev = find_previous_netstat (self, ns->iface);

      if (prev != NULL)
        {
          gint64 rx = 0;
          gint64 tx = 0;

          if (prev->rx_bytes > ns->rx_bytes)
            rx = ns->rx_bytes + (G_MAXUINT64 - prev->rx_bytes);
          else
            rx = ns->rx_bytes - prev->rx_bytes;

          if (prev->tx_bytes > ns->tx_bytes)
            rx = ns->tx_bytes + (G_MAXUINT64 - prev->tx_bytes);
          else
            rx = ns->tx_bytes - prev->tx_bytes;

          if (is_wan (ns->iface))
            {
              info->wan_rx += rx;
              info->wan_tx += tx;
            }
          else if (is_wifi (ns->iface))
            {
              info->wifi_rx += rx;
              info->wifi_tx += tx;
            }
          else
            {
              info->lan_rx += rx;
              info->lan_tx += tx;
            }
        }
    }

  g_clear_pointer (&self->netstat, g_array_unref);
  self->netstat = g_steal_pointer (&netstat);

  return TRUE;
}

static gboolean
usage_linux_system_poll_info (UsageSystem      *sys,
                              UsageSystemInfo  *info,
                              GError          **error)
{
  UsageLinuxSystem *self = (UsageLinuxSystem *)sys;

  g_assert (USAGE_IS_LINUX_SYSTEM (self));
  g_assert (info != NULL);

  memset (info, 0, sizeof *info);

  if (!get_mem_info (self, info) ||
      !get_disk_info (self, info) ||
      !get_net_info (self, info))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "Failed to poll system");
      return FALSE;
    }

  return TRUE;
}

static GArray *
usage_linux_system_list_pids (UsageSystem  *sys,
                              GError      **error)
{
  g_autoptr(GArray) ar = NULL;
  g_autoptr(GDir) dir = NULL;
  const gchar *name;

  dir = g_dir_open ("/proc", 0, error);
  if (dir == NULL)
    return NULL;

  ar = g_array_new (FALSE, FALSE, sizeof (GPid));

  while ((name = g_dir_read_name (dir)))
    {
      if (g_ascii_isdigit (*name))
        {
          GPid pid = g_ascii_strtoll (name, NULL, 10);
          g_array_append_val (ar, pid);
        }
    }

  return g_steal_pointer (&ar);
}

static void
usage_linux_system_finalize (GObject *object)
{
  UsageLinuxSystem *self = (UsageLinuxSystem *)object;

  g_clear_pointer (&self->diskstat, g_array_unref);
  g_clear_pointer (&self->netstat, g_array_unref);

  usage_fd_pool_release ("/proc/meminfo");
  usage_fd_pool_release ("/proc/diskstats");
  usage_fd_pool_release ("/proc/net/dev");

  G_OBJECT_CLASS (usage_linux_system_parent_class)->finalize (object);
}

static void
usage_linux_system_class_init (UsageLinuxSystemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  UsageSystemClass *system_class = USAGE_SYSTEM_CLASS (klass);

  object_class->finalize = usage_linux_system_finalize;

  system_class->poll_info = usage_linux_system_poll_info;
  system_class->list_pids = usage_linux_system_list_pids;
}

static void
usage_linux_system_init (UsageLinuxSystem *self)
{
  UsageAppInstanceProvider *app_instance_provider;

  /* Add Flatpak app instance provider */
  app_instance_provider = usage_flatpak_app_instance_provider_new ();
  usage_system_add_app_instance_provider (USAGE_SYSTEM (self), app_instance_provider);
  g_clear_object (&app_instance_provider);
}

GArray *
usage_linux_system_poll_diskstat (UsageLinuxSystem *self)
{
  g_autoptr(GArray) ar = NULL;
  gchar buf[4096];
  UsageLineReader reader;
  gchar *line;
  gssize len;
  gsize line_len;
  gint diskstat_fd;
  gboolean ret = FALSE;

  g_return_val_if_fail (USAGE_IS_LINUX_SYSTEM (self), NULL);

  /* Open the meminfo file and seek to 0 to reset */
  diskstat_fd = usage_fd_pool_get ("/proc/diskstats", O_RDONLY, TRUE);

  /* Read head of meminfo, as it contains what we need */
  len = read (diskstat_fd, buf, sizeof buf - 1);
  if (len < 0)
    goto failure;

  g_assert (len < (gssize)sizeof buf);

  ar = g_array_new (FALSE, FALSE, sizeof (UsageLinuxDiskStat));

  usage_line_reader_init (&reader, buf, len);
  while ((line = usage_line_reader_next (&reader, &line_len)))
    {
      UsageLinuxDiskStat ds = {0};
      gint64 dummy = 0;
      gint column = DISKSTAT_MAJOR;

      line[line_len] = 0;

      /* Skip past initial space */
      while (g_ascii_isspace (*line))
        line++;

      for (const gchar *ptr = line; *ptr; ptr++)
        {
          gchar ch;

          /* Skip past space and advance to next column */
          if (g_ascii_isspace (*ptr))
            {
              while (g_ascii_isspace (*ptr))
                ptr++;
              column++;
            }

          ch = *ptr;

          switch (column)
            {
            default:
              dummy = ADD_FROM_CHAR (dummy, ch);
              break;

            case DISKSTAT_MAJOR:
              ds.major = ADD_FROM_CHAR (ds.major, ch);
              break;

            case DISKSTAT_MINOR:
              ds.minor = ADD_FROM_CHAR (ds.minor, ch);
              break;

            case DISKSTAT_NAME:
              {
                guint j;
                for (j = 0; j < sizeof ds.device && ds.device[j] != 0; j++)
                  { /* Do Nothing */ }
                if (j < sizeof ds.device)
                  ds.device[j] = ch;
                ds.device[sizeof ds.device - 1] = 0;
                break;
              }

            case DISKSTAT_READS_TOTAL:
              ds.reads_total = ADD_FROM_CHAR (ds.reads_total, ch);
              break;

            case DISKSTAT_READS_MERGED:
              ds.reads_merged = ADD_FROM_CHAR (ds.reads_merged, ch);
              break;

            case DISKSTAT_READS_SECTORS:
              ds.reads_sectors = ADD_FROM_CHAR (ds.reads_sectors, ch);
              break;

            case DISKSTAT_READS_MSEC:
              ds.reads_msec = ADD_FROM_CHAR (ds.reads_msec, ch);
              break;

            case DISKSTAT_WRITES_TOTAL:
              ds.writes_total = ADD_FROM_CHAR (ds.writes_total, ch);
              break;

            case DISKSTAT_WRITES_MERGED:
              ds.writes_merged = ADD_FROM_CHAR (ds.writes_merged, ch);
              break;

            case DISKSTAT_WRITES_SECTORS:
              ds.writes_sectors = ADD_FROM_CHAR (ds.writes_sectors, ch);
              break;

            case DISKSTAT_WRITES_MSEC:
              ds.writes_msec = ADD_FROM_CHAR (ds.writes_msec, ch);
              break;

            case DISKSTAT_IOPS_ACTIVE:
              ds.iops_active = ADD_FROM_CHAR (ds.iops_active, ch);
              break;

            case DISKSTAT_IOPS_MSEC:
              ds.iops_msec = ADD_FROM_CHAR (ds.iops_msec, ch);
              break;

            case DISKSTAT_IOPS_MSEC_WEIGHTED:
              ds.iops_msec_weighted = ADD_FROM_CHAR (ds.iops_msec_weighted, ch);
              break;
            }
        }

      g_strstrip (ds.device);
      g_array_append_val (ar, ds);
    }

  ret = TRUE;

failure:
  usage_fd_pool_put ("/proc/diskstats", diskstat_fd);

  return ret ? g_steal_pointer (&ar) : NULL;
}

GArray *
usage_linux_system_poll_netstat (UsageLinuxSystem *self)
{
  g_autoptr(GArray) ar = NULL;
  gchar buf[4096];
  UsageLineReader reader;
  gchar *line;
  gssize len;
  gsize line_len;
  gint netstat_fd;
  gboolean ret = FALSE;

  g_return_val_if_fail (USAGE_IS_LINUX_SYSTEM (self), NULL);

  /* Get the netstat FD and seek to 0 to reset */
  netstat_fd = usage_fd_pool_get ("/proc/net/dev", O_RDONLY, TRUE);
  if (netstat_fd == -1)
    return NULL;

  /* Read head of meminfo, as it contains what we need */
  len = read (netstat_fd, buf, sizeof buf - 1);
  if (len < 0)
    goto failure;

  g_assert (len < (gssize)sizeof buf);

  ar = g_array_new (FALSE, FALSE, sizeof (UsageLinuxNetStat));

  usage_line_reader_init (&reader, buf, len);

  /* Skip first two lines */
  line = usage_line_reader_next (&reader, &line_len);
  line = usage_line_reader_next (&reader, &line_len);

  while ((line = usage_line_reader_next (&reader, &line_len)))
    {
      UsageLinuxNetStat ns = {0};
      gchar *ptr = line;
      gchar *name;

      line[line_len] = 0;

      for (; *ptr && g_ascii_isspace (*ptr); ptr++) { /* Do Nothing */ }
      name = ptr;
      for (; *ptr && *ptr != ':'; ptr++) { /* Do Nothing */ }
      *ptr = 0;

      ptr++;

      sscanf (ptr,
              "%"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT
              " %"G_GUINT64_FORMAT,
              &ns.rx_bytes,
              &ns.rx_packets,
              &ns.rx_errors,
              &ns.rx_dropped,
              &ns.rx_fifo,
              &ns.rx_frame,
              &ns.rx_compressed,
              &ns.rx_multicast,
              &ns.tx_bytes,
              &ns.tx_packets,
              &ns.tx_errors,
              &ns.tx_dropped,
              &ns.tx_fifo,
              &ns.tx_collisions,
              &ns.tx_carrier,
              &ns.tx_compressed);

      g_strlcpy (ns.iface, name, sizeof ns.iface);

      g_array_append_val (ar, ns);
    }

  ret = TRUE;

failure:

  return ret ? g_steal_pointer (&ar) : NULL;
}

UsageSystem *
usage_system_get_default (void)
{
  static UsageSystem *instance;

  if (g_once_init_enter (&instance))
    {
      UsageSystem *created = usage_linux_system_new ();
      g_object_add_weak_pointer (G_OBJECT (created), (gpointer *)&instance);
      g_once_init_leave (&instance, created);
    }

  return instance;
}
