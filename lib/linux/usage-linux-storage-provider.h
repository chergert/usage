/* usage-linux-storage-provider.h
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "usage-storage-provider.h"

G_BEGIN_DECLS

#define USAGE_TYPE_LINUX_STORAGE_PROVIDER (usage_linux_storage_provider_get_type())

G_DECLARE_FINAL_TYPE (UsageLinuxStorageProvider, usage_linux_storage_provider, USAGE, LINUX_STORAGE_PROVIDER, GObject)

G_END_DECLS
