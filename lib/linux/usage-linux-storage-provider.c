/* usage-linux-storage-provider.c
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "usage-linux-storage-provider"

#include "config.h"

#include "usage-linux-disks-model.h"
#include "usage-linux-storage-provider.h"

struct _UsageLinuxStorageProvider
{
  GObject parent_instance;
};

static void
usage_linux_storage_provider_list_disks_async (UsageStorageProvider *provider,
                                               GCancellable         *cancellable,
                                               GAsyncReadyCallback   callback,
                                               gpointer              user_data)
{
  g_autoptr(UsageLinuxDisksModel) model = NULL;
  g_autoptr(GTask) task = NULL;
  g_autoptr(GError) error = NULL;

  g_assert (USAGE_IS_LINUX_STORAGE_PROVIDER (provider));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  model = usage_linux_disks_model_new ();

  task = g_task_new (provider, cancellable, callback, user_data);
  g_task_set_source_tag (task, usage_linux_storage_provider_list_disks_async);

  if (usage_linux_disks_model_start (model, &error))
    g_task_return_pointer (task, g_steal_pointer (&model), g_object_unref);
  else
    g_task_return_error (task, g_steal_pointer (&error));
}

static GListModel *
usage_linux_storage_provider_list_disks_finish (UsageStorageProvider  *provider,
                                                GAsyncResult          *result,
                                                GError               **error)
{
  g_assert (USAGE_IS_LINUX_STORAGE_PROVIDER (provider));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
storage_provider_iface_init (UsageStorageProviderInterface *iface)
{
  iface->list_disks_async = usage_linux_storage_provider_list_disks_async;
  iface->list_disks_finish = usage_linux_storage_provider_list_disks_finish;
}

G_DEFINE_TYPE_WITH_CODE (UsageLinuxStorageProvider, usage_linux_storage_provider, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (USAGE_TYPE_STORAGE_PROVIDER, storage_provider_iface_init))

static void
usage_linux_storage_provider_class_init (UsageLinuxStorageProviderClass *klass)
{
}

static void
usage_linux_storage_provider_init (UsageLinuxStorageProvider *self)
{
}
