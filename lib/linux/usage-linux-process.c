/* usage-linux-process.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <ctype.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "usage-linux-process.h"
#include "usage-fd-pool-private.h"

struct _UsageLinuxProcess
{
  GObject parent_instance;
  GPid    pid;
  gint64  last_poll;
  gint64  last_utime;
  gint64  last_stime;
  gchar   path[32];
};

enum {
  PROC_PID_STAT_PID,
  PROC_PID_STAT_COMM,
  PROC_PID_STAT_STATE,
  PROC_PID_STAT_PPID,
  PROC_PID_STAT_PGRP,
  PROC_PID_STAT_SESSION,
  PROC_PID_STAT_TTY_NR,
  PROC_PID_STAT_TPGID,
  PROC_PID_STAT_FLAGS,
  PROC_PID_STAT_MINFLT,
  PROC_PID_STAT_CMINFLT,
  PROC_PID_STAT_MAJFLT,
  PROC_PID_STAT_CMAJFLT,
  PROC_PID_STAT_UTIME,
  PROC_PID_STAT_STIME,
  PROC_PID_STAT_CUTIME,
  PROC_PID_STAT_CSTIME,
  PROC_PID_STAT_PRIORITY,
  PROC_PID_STAT_NICE,
  PROC_PID_STAT_NUM_THREADS,
  PROC_PID_STAT_ITREALVALUE,
  PROC_PID_STAT_STARTIME,
  PROC_PID_STAT_VSIZE,
  PROC_PID_STAT_RSS,
  PROC_PID_STAT_RSSLIM,
  PROC_PID_STAT_STARTCODE,
  PROC_PID_STAT_ENDCODE,
  PROC_PID_STAT_STARTSTACK,
  PROC_PID_STAT_KSTKESP,
  PROC_PID_STAT_KSTKEIP,
  PROC_PID_STAT_SIGNAL,
  PROC_PID_STAT_BLOCKED,
  PROC_PID_STAT_SIGIGNORE,
  PROC_PID_STAT_SIGCATCH,
  PROC_PID_STAT_WCHAN,
  PROC_PID_STAT_NSWAP,
  PROC_PID_STAT_CNSWAP,
  PROC_PID_STAT_EXIT_SIGNAL,
  PROC_PID_STAT_PROCESSOR,
  PROC_PID_STAT_RT_PRIORITY,
  PROC_PID_STAT_POLICY,
  PROC_PID_STAT_DELAYACCT_BLKIO_TICKS,
  PROC_PID_STAT_GUEST_TIME,
  PROC_PID_STAT_CGUEST_TIME,
  PROC_PID_STAT_START_DATA,
  PROC_PID_STAT_END_DATA,
  PROC_PID_STAT_START_BRK,
  PROC_PID_STAT_ARG_START,
  PROC_PID_STAT_ARG_END,
  PROC_PID_STAT_ENV_START,
  PROC_PID_STAT_ENV_END,
  PROC_PID_STAT_EXIT_CODE,
};

static void process_iface_init (UsageProcessInterface *iface);

G_DEFINE_TYPE_WITH_CODE (UsageLinuxProcess, usage_linux_process, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (USAGE_TYPE_PROCESS, process_iface_init))

static gdouble _sc_clk_tck;

UsageProcess *
usage_linux_process_new_for_pid (GPid pid)
{
  UsageLinuxProcess *self;

  g_return_val_if_fail (pid != 0, NULL);

  self = g_object_new (USAGE_TYPE_LINUX_PROCESS, NULL);
  self->pid = pid;
  g_snprintf (self->path, sizeof self->path, "/proc/%u/stat", pid);

  return USAGE_PROCESS (self);
}

static GPid
usage_linux_process_get_pid (UsageProcess *process)
{
  return USAGE_LINUX_PROCESS (process)->pid;
}

static void
usage_linux_process_finalize (GObject *object)
{
  UsageLinuxProcess *self = (UsageLinuxProcess *)object;

  usage_fd_pool_release (self->path);

  G_OBJECT_CLASS (usage_linux_process_parent_class)->finalize (object);
}

static void
usage_linux_process_class_init (UsageLinuxProcessClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = usage_linux_process_finalize;

  if (!(_sc_clk_tck = sysconf (_SC_CLK_TCK)))
    _sc_clk_tck = 100;
}

static void
usage_linux_process_init (UsageLinuxProcess *self)
{
}

static gboolean
handle_stat_column (UsageLinuxProcess *self,
                    UsageProcessInfo  *info,
                    const gchar       *word,
                    gint               column,
                    gdouble            duration)
{
  gint64 vi64;

  switch (column)
    {
    case PROC_PID_STAT_STATE:
      switch (*word)
        {
        case 'R':
          info->state = USAGE_PROCESS_STATE_RUNNING;
          break;

        case 'S':
          info->state = USAGE_PROCESS_STATE_SLEEPING;
          break;

        case 'D':
        case 'W': /* Waking 2.6.33+ */
        case 'K': /* 2.6.33 to 3.13 */
          info->state = USAGE_PROCESS_STATE_WAITING;
          break;

        case 'Z':
          info->state = USAGE_PROCESS_STATE_ZOMBIE;
          break;

        case 'T': /* 2.6.33+ */
        case 't': /* 2.6.33+ */
        case 'P': /* 3.9 to 3.13 */
          info->state = USAGE_PROCESS_STATE_STOPPED;
          break;

        case 'X': /* 2.6.0+ */
        case 'x': /* 2.6.33 to 3.13 */
          info->state = USAGE_PROCESS_STATE_DEAD;
          break;

        default:
          info->state = USAGE_PROCESS_STATE_RUNNING;
          break;
        }
      break;

    case PROC_PID_STAT_NICE:
      info->nice = g_ascii_strtoll (word, NULL, 10);
      break;

    case PROC_PID_STAT_PROCESSOR:
      info->processor = g_ascii_strtoll (word, NULL, 10);
      break;

    case PROC_PID_STAT_RT_PRIORITY:
      info->rt_priority = g_ascii_strtoll (word, NULL, 10);
      break;

    case PROC_PID_STAT_STIME:
      vi64 = g_ascii_strtoll (word, NULL, 10);
      if (self->last_poll > 0)
        info->cpu_system = (vi64 - self->last_stime) / (duration * _sc_clk_tck);
      self->last_stime = vi64;
      break;

    case PROC_PID_STAT_UTIME:
      vi64 = g_ascii_strtoll (word, NULL, 10);
      if (self->last_poll > 0)
        info->cpu_user = (vi64 - self->last_utime) / (duration * _sc_clk_tck);
      self->last_utime = vi64;
      break;

    case PROC_PID_STAT_PGRP:
      info->group = g_ascii_strtoll (word, NULL, 10);
      break;

    default:
      break;
    }

  /* rt_priority is the last column we care about */
  return column < PROC_PID_STAT_RT_PRIORITY;
}

static gboolean
get_proc_pid_stat_info (UsageLinuxProcess *self,
                        UsageProcessInfo  *info)
{
  gchar buf[1024];
  gchar *bufptr = buf;
  gboolean ret = FALSE;
  gdouble duration;
  gint64 last_poll;
  gssize len;
  gint column = 0;
  gint stat_fd;

  g_assert (USAGE_IS_LINUX_PROCESS (self));
  g_assert (info != NULL);

  /* Get the proc file FD after seeking to 0 (to reset) */
  stat_fd = usage_fd_pool_get (self->path, O_RDONLY, TRUE);
  if (stat_fd == -1)
    return FALSE;

  last_poll = g_get_monotonic_time ();

  len = read (stat_fd, buf, sizeof buf - 1);
  if (len > 0)
    buf[len] = 0;
  else
    goto failure;

  /* Determine the duration in seconds as a double */
  if (self->last_poll > 0)
    duration = (last_poll - self->last_poll) / (gdouble)G_USEC_PER_SEC;
  else
    duration = 0;

  do
    {
      gchar *endptr = bufptr;
      gchar saved;

      /* Skip past any spaces */
      while (*endptr != 0 && isspace (*endptr ))
        endptr++;

      /* Set the word start to here */
      bufptr = endptr;

      /* Set endptr to the position after the current word */
      for (endptr = bufptr; *endptr && !isspace (*endptr); endptr++) { }

      /* Switch trailing char to \0 so we can use it as a cstring */
      saved = *endptr;
      *endptr = 0;

      if (!handle_stat_column (self, info, bufptr, column, duration))
        break;

      column++;

      *endptr = saved;

      /* Skip past space after word */
      for (; *endptr != 0 && isspace (*endptr); endptr++) { }

      bufptr = endptr;
    }
  while (*bufptr != 0);

  self->last_poll = last_poll;

  ret = TRUE;

failure:
  usage_fd_pool_put (self->path, stat_fd);

  return ret;
}

static gboolean
usage_linux_process_poll_info (UsageProcess      *process,
                               UsageProcessInfo  *info,
                               GError           **error)
{
  UsageLinuxProcess *self = (UsageLinuxProcess *)process;

  g_assert (USAGE_IS_LINUX_PROCESS (self));
  g_assert (info != NULL);

  memset (info, 0, sizeof *info);

  if (!get_proc_pid_stat_info (self, info))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "Failed to read data from proc");
      return FALSE;
    }

  return TRUE;
}

static void
process_iface_init (UsageProcessInterface *iface)
{
  iface->get_pid = usage_linux_process_get_pid;
  iface->poll_info = usage_linux_process_poll_info;
}
