/* usage-version-macros.h
 *
 * Copyright 2018-2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

#include "usage-version.h"

#ifndef _USAGE_EXTERN
#define _USAGE_EXTERN extern
#endif

#ifdef USAGE_DISABLE_DEPRECATION_WARNINGS
#define USAGE_DEPRECATED _USAGE_EXTERN
#define USAGE_DEPRECATED_FOR(f) _USAGE_EXTERN
#define USAGE_UNAVAILABLE(maj,min) _USAGE_EXTERN
#else
#define USAGE_DEPRECATED G_DEPRECATED _USAGE_EXTERN
#define USAGE_DEPRECATED_FOR(f) G_DEPRECATED_FOR(f) _USAGE_EXTERN
#define USAGE_UNAVAILABLE(maj,min) G_UNAVAILABLE(maj,min) _USAGE_EXTERN
#endif

#define USAGE_VERSION_3_38 (G_ENCODE_VERSION (3, 38))

#if (USAGE_MINOR_VERSION == 99)
# define USAGE_VERSION_CUR_STABLE (G_ENCODE_VERSION (USAGE_MAJOR_VERSION + 1, 0))
#elif (USAGE_MINOR_VERSION % 2)
# define USAGE_VERSION_CUR_STABLE (G_ENCODE_VERSION (USAGE_MAJOR_VERSION, USAGE_MINOR_VERSION + 1))
#else
# define USAGE_VERSION_CUR_STABLE (G_ENCODE_VERSION (USAGE_MAJOR_VERSION, USAGE_MINOR_VERSION))
#endif

#if (USAGE_MINOR_VERSION == 99)
# define USAGE_VERSION_PREV_STABLE (G_ENCODE_VERSION (USAGE_MAJOR_VERSION + 1, 0))
#elif (USAGE_MINOR_VERSION % 2)
# define USAGE_VERSION_PREV_STABLE (G_ENCODE_VERSION (USAGE_MAJOR_VERSION, USAGE_MINOR_VERSION - 1))
#else
# define USAGE_VERSION_PREV_STABLE (G_ENCODE_VERSION (USAGE_MAJOR_VERSION, USAGE_MINOR_VERSION - 2))
#endif

/**
 * USAGE_VERSION_MIN_REQUIRED:
 *
 * A macro that should be defined by the user prior to including
 * the ide.h header.
 *
 * The definition should be one of the predefined IDE version
 * macros: %USAGE_VERSION_3_38, ...
 *
 * This macro defines the lower bound for the Builder API to use.
 *
 * If a function has been deprecated in a newer version of Builder,
 * it is possible to use this symbol to avoid the compiler warnings
 * without disabling warning for every deprecated function.
 *
 * Since: 3.38
 */
#ifndef USAGE_VERSION_MIN_REQUIRED
# define USAGE_VERSION_MIN_REQUIRED (USAGE_VERSION_CUR_STABLE)
#endif

/**
 * USAGE_VERSION_MAX_ALLOWED:
 *
 * A macro that should be defined by the user prior to including
 * the ide.h header.

 * The definition should be one of the predefined Builder version
 * macros: %USAGE_VERSION_1_0, %USAGE_VERSION_1_2,...
 *
 * This macro defines the upper bound for the IDE API to use.
 *
 * If a function has been introduced in a newer version of Builder,
 * it is possible to use this symbol to get compiler warnings when
 * trying to use that function.
 *
 * Since: 3.38
 */
#ifndef USAGE_VERSION_MAX_ALLOWED
# if USAGE_VERSION_MIN_REQUIRED > USAGE_VERSION_PREV_STABLE
#  define USAGE_VERSION_MAX_ALLOWED (USAGE_VERSION_MIN_REQUIRED)
# else
#  define USAGE_VERSION_MAX_ALLOWED (USAGE_VERSION_CUR_STABLE)
# endif
#endif

#if USAGE_VERSION_MAX_ALLOWED < USAGE_VERSION_MIN_REQUIRED
#error "USAGE_VERSION_MAX_ALLOWED must be >= USAGE_VERSION_MIN_REQUIRED"
#endif
#if USAGE_VERSION_MIN_REQUIRED < USAGE_VERSION_3_38
#error "USAGE_VERSION_MIN_REQUIRED must be >= USAGE_VERSION_3_38"
#endif

#define USAGE_AVAILABLE_IN_ALL                   _USAGE_EXTERN

#if USAGE_VERSION_MIN_REQUIRED >= USAGE_VERSION_3_38
# define USAGE_DEPRECATED_IN_3_38                USAGE_DEPRECATED
# define USAGE_DEPRECATED_IN_3_38_FOR(f)         USAGE_DEPRECATED_FOR(f)
#else
# define USAGE_DEPRECATED_IN_3_38                _USAGE_EXTERN
# define USAGE_DEPRECATED_IN_3_38_FOR(f)         _USAGE_EXTERN
#endif

#if USAGE_VERSION_MAX_ALLOWED < USAGE_VERSION_3_38
# define USAGE_AVAILABLE_IN_3_38                 USAGE_UNAVAILABLE(3, 38)
#else
# define USAGE_AVAILABLE_IN_3_38                 _USAGE_EXTERN
#endif
