/* usage-util.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "usage-util.h"

static gboolean
is_flatpak (void)
{
  static gboolean _is_flatpak;
  static gboolean did_init;

  if (!did_init)
    {
      _is_flatpak = g_file_test ("/.flatpak-info", G_FILE_TEST_EXISTS);
      did_init = TRUE;
    }

  return _is_flatpak;
}

gchar **
usage_util_build_argv (const gchar *first,
                       ...)
{
  GPtrArray *ar = g_ptr_array_new ();
  va_list args;

  if (is_flatpak ())
    {
      g_ptr_array_add (ar, g_strdup ("flatpak-spawn"));
      g_ptr_array_add (ar, g_strdup ("--host"));
    }

  va_start (args, first);
  do
    {
      g_ptr_array_add (ar, g_strdup (first));
    }
  while ((first = va_arg (args, const gchar *)));
  va_end (args);

  g_ptr_array_add (ar, NULL);

  return (gchar **)g_ptr_array_free (ar, FALSE);
}
