/* usage-system-info.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

typedef struct _UsageSystemInfo
{
  guint64 mem_total;
  guint64 mem_available;
  guint64 mem_cache;
  guint64 swap_total;
  guint64 swap_free;
  guint64 disk_read;
  guint64 disk_write;
  guint64 lan_rx;
  guint64 lan_tx;
  guint64 wan_rx;
  guint64 wan_tx;
  guint64 wifi_rx;
  guint64 wifi_tx;
} UsageSystemInfo;

G_END_DECLS
