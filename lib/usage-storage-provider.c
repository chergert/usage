/* usage-storage-provider.c
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "usage-storage-provider"

#include "config.h"

#include "usage-storage-provider.h"

G_DEFINE_INTERFACE (UsageStorageProvider, usage_storage_provider, G_TYPE_OBJECT)

static void
usage_storage_provider_real_list_disks_async (UsageStorageProvider *self,
                                              GCancellable         *cancellable,
                                              GAsyncReadyCallback   callback,
                                              gpointer              user_data)
{
  g_task_report_new_error (self, callback, user_data,
                           usage_storage_provider_real_list_disks_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Not supported");
}

static GListModel *
usage_storage_provider_real_list_disks_finish (UsageStorageProvider  *self,
                                               GAsyncResult          *result,
                                               GError               **error)
{
  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
usage_storage_provider_default_init (UsageStorageProviderInterface *iface)
{
  iface->list_disks_async = usage_storage_provider_real_list_disks_async;
  iface->list_disks_finish = usage_storage_provider_real_list_disks_finish;
}

/**
 * usage_storage_provider_list_disks_async:
 * @self: a #UsageStorageProvider
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a #GAsyncReadyCallback or %NULL
 * @user_data: closure data for @callback
 *
 * Asynchronously requests the list of disks available on the system.
 *
 * The result of this request can be retreived from @callback using
 * usage_storage_provider_list_disks_finish().
 */
void
usage_storage_provider_list_disks_async (UsageStorageProvider *self,
                                         GCancellable         *cancellable,
                                         GAsyncReadyCallback   callback,
                                         gpointer              user_data)
{
  g_return_if_fail (USAGE_IS_STORAGE_PROVIDER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  USAGE_STORAGE_PROVIDER_GET_IFACE (self)->list_disks_async (self, cancellable, callback, user_data);
}

/**
 * usage_storage_provider_list_disks_finish:
 * @self: a #UsageStorageProvider
 * @result: a #GAsyncResult
 * @error: a location for a #GError or %NULL
 *
 * Completes an asynchronous request to usage_storage_provider_list_disks_async().
 *
 * This function should be called from the callback provided to
 * usage_storage_provider_list_disks_async().
 *
 * The resulting #GListModel will be updated as changes are discovered.
 *
 * Returns: (transfer full): a #GListModel of #UsageDisk
 */
GListModel *
usage_storage_provider_list_disks_finish (UsageStorageProvider  *self,
                                          GAsyncResult          *result,
                                          GError               **error)
{
  g_return_val_if_fail (USAGE_IS_STORAGE_PROVIDER (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);

  return USAGE_STORAGE_PROVIDER_GET_IFACE (self)->list_disks_finish (self, result, error);
}
