/* usage-process-info.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

typedef enum _UsageProcessState
{
  USAGE_PROCESS_STATE_RUNNING,
  USAGE_PROCESS_STATE_SLEEPING,
  USAGE_PROCESS_STATE_WAITING,
  USAGE_PROCESS_STATE_STOPPED,
  USAGE_PROCESS_STATE_DEAD,
  USAGE_PROCESS_STATE_ZOMBIE,
} UsageProcessState;

typedef struct _UsageProcessInfo
{
  UsageProcessState state;
  guint rt_priority;
  gint processor;
  gint nice;
  GPid group;
  guint64 mem_virtual;
  guint64 mem_resident;
  guint64 mem_shared;
  guint64 disk_read;
  guint64 disk_write;
  guint64 net_rx;
  guint64 net_tx;
  gdouble cpu_user;
  gdouble cpu_system;
} UsageProcessInfo;

G_END_DECLS
