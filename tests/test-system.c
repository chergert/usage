#include "libusage.h"

static void
test_system (void)
{
  g_autoptr(GError) error = NULL;
  UsageSystem *sys;
  UsageSystemInfo info = {0};
  gboolean r;

  sys = usage_system_get_default ();
  r = usage_system_poll_info (sys, &info, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_assert_cmpint (info.mem_total, >, 0);
  g_assert_cmpint (info.mem_cache, >, 0);
  g_assert_cmpint (info.mem_available, >, 0);
  g_assert_cmpint (info.swap_free, >, 0);
  g_assert_cmpint (info.swap_total, >, 0);
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Usage/System/basic", test_system);
  return g_test_run ();
}
