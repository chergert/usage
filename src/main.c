/* main.c
 *
 * Copyright 2019 Christian Hergert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "config.h"

#include <libhandy-1/handy.h>
#include <libusage.h>

#include "gnome_usage-window.h"

static void
on_startup (GtkApplication *app)
{
  hdy_init ();
}

static void
on_activate (GtkApplication *app)
{
	GtkWindow *window;

	/* It's good practice to check your parameters at the beginning of the
	 * function. It helps catch errors early and in development instead of
	 * by your users.
	 */
	g_assert (GTK_IS_APPLICATION (app));

	/* Get the current window or create one if necessary. */
	window = gtk_application_get_active_window (app);
	if (window == NULL)
		window = g_object_new (GNOME_USAGE_TYPE_WINDOW,
		                       "application", app,
		                       "default-width", 600,
		                       "default-height", 300,
		                       NULL);

	/* Ask the window manager/compositor to present the window. */
	gtk_window_present (window);
}

static gboolean
print_usage (gpointer data)
{
  UsageSystem *sys = usage_system_get_default ();
  UsageSystemInfo info = {0};
  g_autoptr(GError) error = NULL;

  if (!usage_system_poll_info (sys, &info, &error))
    g_error ("%s", error->message);

  g_print ("%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT","
           "%"G_GSIZE_FORMAT"\n",
           info.mem_total,
           info.mem_available,
           info.mem_cache,
           info.swap_total,
           info.swap_free,
           info.disk_read,
           info.disk_write,
           info.lan_rx,
           info.lan_tx,
           info.wan_rx,
           info.wan_tx,
           info.wifi_rx,
           info.wifi_tx);

  return G_SOURCE_CONTINUE;
}

static void
instances_changed_cb (GListModel *model,
                      guint       pos,
                      guint       removed,
                      guint       added,
                      gpointer    user_data)
{
  for (guint i = 0; i < added; i++)
    {
      g_autoptr(UsageAppInstance) instance = g_list_model_get_item (model, pos + i);

      g_print ("=>>> %s added\n",
               usage_app_instance_get_app_id (instance));
    }
}

static gboolean
print_procs (gpointer data)
{
  UsageSystem *sys = usage_system_get_default ();
  g_autoptr(GArray) pids = usage_system_list_pids (sys, NULL);
  static GHashTable *by_pid;

  if (by_pid == NULL)
    by_pid = g_hash_table_new (NULL, NULL);

  for (guint i = 0; i < pids->len; i++)
    {
      GPid pid = g_array_index (pids, GPid, i);
      UsageProcess *process;
      UsageProcessInfo info;

      if (!(process = g_hash_table_lookup (by_pid, GINT_TO_POINTER (pid))))
        {
          process = usage_process_new_for_pid (pid);
          g_hash_table_insert (by_pid, GINT_TO_POINTER (pid), process);
        }

      if (usage_process_poll_info (process, &info, NULL))
        {
          g_print ("%d: %0.1lf :: %0.1lf%% %0.1lf%%\n", pid,
                   ((info.cpu_user + info.cpu_system) * 100),
                   (info.cpu_user * 100), (info.cpu_system * 100));
        }
    }

  return G_SOURCE_CONTINUE;
}

int
main (int   argc,
      char *argv[])
{
	g_autoptr(GtkApplication) app = NULL;
  UsageSystem *sys = NULL;
  GListModel *instances;
	int ret;

  usage_init ();

  sys = usage_system_get_default ();

	/* Set up gettext translations */
	bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

  g_print ("MemTotal,MemAvailable,MemCache,SwapTotal,SwapFree,DiskRead,DiskWrite,LanRx,LanTx,WanRx,WanTx,WifiRx,WifiTx\n");
  g_timeout_add_seconds (1, print_usage, NULL);
  g_timeout_add_seconds (2, print_procs, NULL);

  instances = usage_system_get_app_instances (sys);

  g_signal_connect (instances,
                    "items-changed",
                    G_CALLBACK (instances_changed_cb),
                    NULL);
  instances_changed_cb (instances, 0, 0, g_list_model_get_n_items (instances), NULL);

	/*
	 * Create a new GtkApplication. The application manages our main loop,
	 * application windows, integration with the window manager/compositor, and
	 * desktop features such as file opening and single-instance applications.
	 */
	app = gtk_application_new ("org.gnome.Usage", G_APPLICATION_FLAGS_NONE);

	/*
	 * We connect to the activate signal to create a window when the application
	 * has been lauched. Additionally, this signal notifies us when the user
	 * tries to launch a "second instance" of the application. When they try
	 * to do that, we'll just present any existing window.
	 *
	 * Because we can't pass a pointer to any function type, we have to cast
	 * our "on_activate" function to a GCallback.
	 */
	g_signal_connect (app, "startup", G_CALLBACK (on_startup), NULL);
	g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);

	/*
	 * Run the application. This function will block until the applicaiton
	 * exits. Upon return, we have our exit code to return to the shell. (This
	 * is the code you see when you do `echo $?` after running a command in a
	 * terminal.
	 *
	 * Since GtkApplication inherits from GApplication, we use the parent class
	 * method "run". But we need to cast, which is what the "G_APPLICATION()"
	 * macro does.
	 */
	ret = g_application_run (G_APPLICATION (app), argc, argv);

  usage_shutdown ();

	return ret;
}
