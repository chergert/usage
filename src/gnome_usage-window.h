/* gnome_usage-window.h
 *
 * Copyright 2019 Christian Hergert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libhandy-1/handy.h>

G_BEGIN_DECLS

#define GNOME_USAGE_TYPE_WINDOW (gnome_usage_window_get_type())

G_DECLARE_FINAL_TYPE (GnomeUsageWindow, gnome_usage_window, GNOME_USAGE, WINDOW, HdyApplicationWindow)

G_END_DECLS
